// ActionScript file
/** MAIN_SCRIPT *****************************************************************************************************************
 *  Contains the source code necessary to run the game. 
 *  Here are the important states referenced by this file that are located in main.mxml. I'm listing
 *  them for viewer's convenience:
 * 			State id=Register (used to login).
 * 			State id=Talk (used to have the player be able to talk to someone
 * 			State id=Info (used to display a custom component that displays dialogue and player info
 * 			State id=Game (the state used to manage game-playing aspects of Flash)
 * For the most part, the Info and Talk states are taken care of. Changes that are made would mainly be done in the Game states.
 * States do not represent different worlds. Instead, all the resources needed to draw all worlds is embedded into the final SWF
 * file and located in ResourceManager.as. The currentWorld variable is responsible for tracking which world the user is currently
 * in, and EntranceObjects are used to manage entering and exiting worlds.
 * 
 * The section outline of this file is:
 * 			Initialization of game variables (includes event dispatchers, initiating drawing resources for the world, etc...
 * 			Event handlers.
 * 			Functions dealing with Multiplayer.
 * 			Graphics functions.
 * 
 * 
 * 
 * ******************************************************************************************************************************/
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextArea;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.ListEvent;
		 
		 	/*default initialization of vars here*/
			private var gameObjectManager:GameObjectManager = null;
			private var messageDispatcher:MessageDispatcher = null;
			private var OPManager:OtherPlayerManager = null;
			private var world:World = null;
			private var scrollX:Number = 0;
			private var scrollY:Number = 0;
			private var currentWorld:int = 1; //defines the current level the player is in (see ResourceManager.as)
			
			private var userID:String = null;
			public var player:Player = null;
			public var player1:Player1 = null;	
			public var player2:Player1 = null;	

			private var lastFrame:Date;
			private var lastWorldScrollX:Number=0;
			private var lastWorldScrollY:Number=0;
			private var lastPlayerPoint:Point = new Point(0,0);
			private var blahblah:int = 0;
			private var OP:OtherPlayer = null;
			
			/*handles login checks*/
			private function CheckLogin():void
			{
					userName.text = this.userID;
					Alert.show("Konichiwa " + userName.text + ", you are logged in.");
					Application.application.removeChild(loginPanel);
					Application.application.currentState="Game";
					initGame(); //start your engines
			}
			
			/*Initialization of graphics and game resources*/
			private function initGame():void
			{
				lastFrame = new Date();
				this.gameObjectManager = new GameObjectManager();
				this.messageDispatcher = new MessageDispatcher();
			
				/*Event listeners need to be added to the application to be registered*/
				Application.application.addEventListener(MouseEvent.CLICK, click);
				Application.application.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
				Application.application.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
				Application.application.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
  			
  				/*Initializes important aspects of the game*/
  				ResourceManager.InitialiseResources();
				ConversationManager.CreateConversations();
				GameProgressEventManager.CreateGameProgressEventManager(this.messageDispatcher);
				
			
  				this.world = ResourceManager.CreateWorld(this.messageDispatcher, this.gameObjectManager, currentWorld);
  					
				var levelWidth:int = this.world.getCols()*this.world.TileWidth
				var levelHeight:int = this.world.getRows()*this.world.TileHeight
				this.OPManager = new OtherPlayerManager(this.world, levelWidth, levelHeight);
				
				this.player = new Player(this.messageDispatcher, this.gameObjectManager, new Point(80, 0), this.world);
				//this.player2 = new Player1(this.messageDispatcher, this.gameObjectManager, new Point(80, 80), this.world, this.scrollX + 40, this.scrollY + 40);
				//this.player1 = new Player1(this.messageDispatcher, this.gameObjectManager, new Point(0, 0), this.world);
				AddCustomEventListeners();
				
				this.addEventListener(Event.ENTER_FRAME, enterFrame);
			}	
			

			private function AddCustomEventListeners():void
			{
				//COMMON EVENT LISTENERS
				messageDispatcher.addEventListener(GameProgressEvent.PLAYER_START_MOVE, playerStartMove);
				messageDispatcher.addEventListener(GameProgressEvent.DISPLAY_ACTIONS, displayActions);
				messageDispatcher.addEventListener(GameProgressEvent.IN_FRONT_OF_PORTAL, displayActions);
				messageDispatcher.addEventListener(GameProgressEvent.ENTER_WORLD, jumpToNewWorld);
				messageDispatcher.addEventListener(GameProgressEvent.EXIT_WORLD, jumpToNewWorld);
				//SPECIFIC EVENT LISTENERS BASED ON WORLD
				if(this.currentWorld==1) 
				{
					
					messageDispatcher.addEventListener(GameProgressEvent.TALK, talk);
					messageDispatcher.addEventListener(GameProgressEvent.END_TALK, endTalk);
				}
				
				if(this.currentWorld==2)
				{
					//messageDispatcher.addEventListener(GameProgressEvent.VIEW_PRICES, displayPrice);
					messageDispatcher.addEventListener(GameProgressEvent.INPUT_SELLER, InputSeller);
				}
			}
			
			/*=============================================================================================== 
			* EVENT HANDLERS
			* This section of code deals with functions that handle specified events such as when the player 
			* approaches an item that can be picked up, encounters a seller, etc. Event listeners are added 
			* in the appComplete function just above for these functions. These functions depend on 
			* the classes GameProgressEvents and InteractionGameObjects (and the classes that extend these 
			* classes) to listen to and handle various events that are fired throughout the game.
			* ===============================================================================================*/ 
			
			/*Here you can control specifically which jump to make to which world, depending
			on whether specific quests or actions have been completed.
			If the specific world is to be entered and can be returned to, then the player's last
			position must be memorized.
			Consequently, when we exit a world, we must call upon the last position remembered to
			return to.*/
			private function jumpToNewWorld(event:GameProgressEvent):void
			{
				if(event.type.toString()=="Action_EnterWorld")
				{   //if entering world, we remember the last location from which current player entered 
					lastWorldScrollX = scrollX;
					lastWorldScrollY = scrollY;
					lastPlayerPoint = this.player.Position;
				}
				
				var eventInfo:int = event.ActionData as int;
				/*takes care of clearing the old world map, objects (including) players.*/
				this.currentWorld = eventInfo;
				this.actionBox.removeAllChildren();
				gameObjectManager.shutdown();
				
				/*recreates objects, players, etc*/
				this.messageDispatcher = new MessageDispatcher();
				this.world = ResourceManager.CreateWorld(this.messageDispatcher, this.gameObjectManager, currentWorld);
				AddCustomEventListeners();
				
				var playerPoint:Point = new Point(0,0);
				if(event.type.toString()=="Action_ExitWorld")
				{   //when exiting, we recreate the position the player left in the old world
					scrollX = lastWorldScrollX;
					scrollY = lastWorldScrollY;
					playerPoint = lastPlayerPoint;
				}	
				
				this.player = new Player(this.messageDispatcher, this.gameObjectManager, playerPoint, this.world);
			}
			
			
			private function playerStartMove(event:GameProgressEvent):void
			{
				this.actionBox.removeAllChildren();
			}
			
			/*function displayActions: Called on when a player nears an object capable of being picked up.
			* Displays each action that can be performed on the object using buttons.*/
			private function displayActions(event:GameProgressEvent):void
			{
				var displayActionEvent:GameProgressEvent = event;
				
				var actions:Array = displayActionEvent.ActionData as Array;
				if (actions != null)
				{
					for each (var action:Object in actions)
					{
						var button:Button = new Button();
						button.label = action.name;
						button.addEventListener(
							MouseEvent.CLICK, 
							function(event:MouseEvent):void
							{
								Application.application.actionBox.removeAllChildren();
								Application.application.messageDispatcher.dispatchEvent(
									new GameProgressEvent(action.event, GameObjectIDs.PLAYER, displayActionEvent.SenderID));															
							}
						);
						this.actionBox.addChild(button);
					}
				}
			}		
			
			/*asks the user to specify whom he would like to buy from*/
			private function InputSeller(event:GameProgressEvent):void
			{
				var inputText:TextArea = new TextArea();	
				
				var button:Button = new Button();
				button.label = "Buy from Guild";
				button.addEventListener(MouseEvent.CLICK, 
					function (event:MouseEvent):void 
					{
							
						Application.application.actionBox.addChild(inputText);	
							
					}
				);
				
				
				this.actionBox.addChild(inputText);
				this.actionBox.addChild(button);
				
			}
			
			
			/* void displayPrice: Handles the event of a user approaching a sales object in the game. Allows the player
			* to view the prices of another user by displaying them in a datagrid. To purchase an item, 
			* the player refers to the ComboBox (a drop down list) and selects the name of appropriate program. */
			private function displayPrice(event:GameProgressEvent):void 
			{
				var PriceData:ArrayCollection = event.ActionData as ArrayCollection;
				//var PriceData:ArrayCollection = new ArrayCollection();
				var button:Button = new Button();
				button.label = "View My Programs!";
				button.addEventListener(MouseEvent.CLICK, 
					function (event:MouseEvent):void
					{/* Must define the event handler within as addEventListener cannot handle extra parameters*/
						
						Application.application.actionBox.removeAllChildren();
						
						/*Setup for columns and dataGrid. Note that this is oddly the only way of setting the column names for the
						DataGrid. No other options exist that are cheaper other than a for-loop through a string array of properties*/
						var columns:Array = [];
						var myColumn:DataGridColumn;
							myColumn = new DataGridColumn( "Name" );
							myColumn.headerText = "Name";
							columns.push( myColumn );
							
							myColumn = new DataGridColumn( "Description");
							myColumn.headerText = "Description";
							myColumn.wordWrap = true;
							columns.push( myColumn);
							
							myColumn = new DataGridColumn( "Price" );
							myColumn.headerText = "Price";
							columns.push( myColumn );
						
						var displayGrid:DataGrid = new DataGrid();
						displayGrid.width = 400;
						displayGrid.columns= columns;
						displayGrid.dataProvider= PriceData;
						
						/*Setup for drop-down list that allows for purchases to be made*/
						var Names:Array = [];
						for(var i:int = 0; i < PriceData.length; i++) { Names.push(PriceData[i].Name); }
						var myComboBox:ComboBox = new ComboBox();
						myComboBox.dataProvider = Names;
						
						Application.application.actionBox.addChild(displayGrid);
						Application.application.actionBox.addChild(myComboBox);
						myComboBox.addEventListener(ListEvent.CHANGE, makePurchase);
					}
				); 
				this.actionBox.addChild(button);
			}
			
			private function makePurchase(event:ListEvent):void
			{
				
				Alert.show(event.currentTarget.selectedItem.toString());
				//TODO:Modify Info menu accordingly.
			}
			
			/* This section deals with initialization and handling of conversation events by working with
			* the ConversationManager and ConversationText classes. The functions themselves are 
			* pretty self explanatory.
			*/
			private function endTalk(event:GameProgressEvent):void
			{
				this.conversationWindow.text = "";
				this.conversationResponseBox.removeAllChildren();
				this.currentState = "Game";				
			}
			
			private function talk(event:GameProgressEvent):void
			{				
				
				var conversationID:int = event.ActionData.conversationId;
				var name:String = event.ActionData.name;
				var sender:int = event.SenderID;
				Alert.show(conversationID.toString());
				var conversation:Conversation = ConversationManager.GetConversation(conversationID);
				if (conversation != null)
				{
					var conversationText:ConversationText = conversation.getConversationEntryPoint();
					
					if (conversationText != null)
					{					
						this.currentState = "Talk";
						this.talkPanel.title = name;
						this.displayConversationText(conversationText, sender);
					}
				}
			}
			
			private function displayConversationText(conversationText:ConversationText, sender:int):void
			{
				this.conversationResponseBox.removeAllChildren();
				this.conversationWindow.text += conversationText.Text + "\n\n";
				for each (var responseID:int in conversationText.ResponseIDs)
				{
					var conversationResponse:ConversationResponse = 
						conversationText.ParentConversation.getConversationResponse(responseID);
					
					if (conversationResponse != null)
						displayConversationResponse(conversationResponse, sender);
				}
			}
			
			private function displayConversationResponse(conversationResponse:ConversationResponse, sender:int):void
			{
				var application:main = this;
				
				
				var button:Button = new Button();
				button.label = conversationResponse.Text;
				button.addEventListener
				(
					MouseEvent.CLICK, 
					function (event:MouseEvent):void
					{
						if (conversationResponse.ConversationID == Conversation.END_CONVERSATION)
						{
							application.messageDispatcher.dispatchEvent(
								new GameProgressEvent(GameProgressEvent.END_TALK, sender));
						}
						else
						{
							application.conversationWindow.text += "You said: " + conversationResponse.Text + "\n\n";
							application.displayConversationText(
								conversationResponse.ParentConversation.getConversationText(
									conversationResponse.ConversationID									
								),
								sender
							);
						}
					}
				);
				this.conversationResponseBox.addChild(button);
			}	
			
			/*Mouse event handlers. Action listeners added upon entering into the game state.
			* Each function calls on a gameObjectManager function, which redraws the map
			* according to how the user clicked. */
			private function click(event:MouseEvent):void
		    {
		    	
		    	if (event.target == this.canvas)
		    		this.gameObjectManager.click(event, int(scrollX), int(scrollY));
		    }
		    
		    private function mouseDown(event:MouseEvent):void
			{
				if (event.target == this.canvas)
		    		this.gameObjectManager.mouseDown(event, int(scrollX), int(scrollY));
		    }
		    
		    private function mouseUp(event:MouseEvent):void
			{
				if (event.target == this.canvas)
		    		this.gameObjectManager.mouseUp(event, int(scrollX), int(scrollY));
		    }
		    
		    private function mouseMove(event:MouseEvent):void
			{
				if (event.target == this.canvas)
		    		this.gameObjectManager.mouseMove(event, int(scrollX), int(scrollY));
		    }	
		    
		    
		    /* key functions implemented but contains a weird geoemtric error; hence they are disabled
		    private function keyDownHandle(event:KeyboardEvent):void
			{	//DISABLED FUNCTION
				if (event.target == this.canvas)
					this.gameObjectManager.keyDownHandle(event, int(scrollX), int(scrollY));
			}
			*/
			
			/*=============================================================================================== 
			* GRAPHICAL DATA HANDLERS
			* This section of code deals with functions that specify control of the world and the viewing 
			* window options. Functions here are used to draw different levels and their graphics.
			* Functions here also take care of mouse clicks. This section can be added onto to extend controls
			* or to add new graphical features to the screen.
			* ===============================================================================================*/ 
			
			/*EnterFrame:
				Draws the map onto the canvas and limits the world scrolling.
				Also introduces a timeStamp.*/
			private function enterFrame(event:Event):void
			{	
				
				// Calculate the time since the last frame
				var thisFrame:Date = new Date();
				var seconds:Number = (thisFrame.getTime() - lastFrame.getTime())/1000.0;
		    	lastFrame = thisFrame;
				
				var canvasGraphics:Graphics = this.canvas.graphics;	
				var surface:BitmapData = new BitmapData(Application.application.width, Application.application.height, true);
				
				// player starts every map at upper left hand corner of map
				this.scrollX=0;
				this.scrollY=0;
				
				//scroll only if the map size is greater than that of the window
				if(world.Tiles[0].length > this.width/world.TileWidth
		    		&& world.Tiles[0][0].length > this.height/world.TileHeight)
				{
					//centre on the player
					scrollX = this.player.Position.x - this.width / 2;
					scrollY = this.player.Position.y - this.height / 2;
					
			    	// limit the scrolling
			    	var maxXScroll:int = (world.TileWidth * world.Tiles[0].length) - this.width;
			    	var maxYScroll:int = (world.TileHeight * world.Tiles[0][0].length) - this.height;
			  		
			    	if (scrollX < 0) scrollX = 0;
			    	else if (scrollX > maxXScroll) scrollX = maxXScroll;
			    	
			    	if (scrollY < 0) scrollY = 0;
			    	else if (scrollY > maxYScroll) scrollY = maxYScroll;
			    }
		    	
		    	// update the game objects
		    	this.gameObjectManager.enterFrame(seconds);
				
				// draw the ground layer
				world.drawWorldLayer(0, surface, int(scrollX), int(scrollY));
				
				// draw the player layer
				this.gameObjectManager.drawObjects(surface, int(scrollX), int(scrollY));
				
				// draw the remaining layers
				for (var layers:int = 1; layers < world.Tiles.length; ++layers)
					world.drawWorldLayer(layers, surface, int(scrollX), int(scrollY));							
				
				canvasGraphics.clear();
	    		canvasGraphics.beginBitmapFill(surface, null, false, false);
	    		canvasGraphics.drawRect(0, 0, this.canvas.width, this.canvas.height);
	    		canvasGraphics.endFill();
	    		
	    		this.blahblah++;
			}	
		
