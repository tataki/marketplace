// Actionscript
package
{
	import flash.events.Event;

	/**=====================================================================================================================
	COMET2MANAGEREVENT CLASS
	This class defines the string constants that are to be used for any class using the COMET model to connect to the server.
	This class works with Comet2ManagerImpl (which extends EventDispatcher) to establish the eventdispatch-event-reaction
	model typical in Actionscript code.
	=====================================================================================================================*/
	
	public class Comet2ManagerEvent extends Event
	{
		// EVENT TYPES
		public static const COMET_REQUEST_SENT:String = "COMET_REQUEST_SENT";
		public static const COMET_REQUEST_ERROR:String = "COMET_REQUEST_ERROR";
		public static const COMET_DATA_RECEIVED:String = "COMET_DATA_RECEIVED";
		public static const COMET_CONNECTION_CLOSED:String = "COMET_CONNECTION_CLOSED";
		
		// FIELDS
		public var content:Object;
		
		// CONSTRUCTOR
		public function Comet2ManagerEvent(type:String, content:Object=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.content = content;
		}
		
		// HELPERS
		override public function clone():Event {
			return new Comet2ManagerEvent(this.type,this.content,this.bubbles,this.cancelable);
		}
	}
}

