package
{
	import flash.display.*;
	import flash.events.*;
	import flash.geom.Point;
	
	/**===============================================================================
	CLASS BASEOBJECTS 
	This class defines the basic properties of all objects. 
	Should not be modified unless you plan to also update the changes for all 
	extended classes that define game objects as well.
	Making changes to this class means making changes to:
		GameObject.as
		GameObjectManager.as
		subsequent classes that extend GameObject.as
		
	Most functions that are written are empty: they serve as templates
	for future classes to override and define an object's own individual behavior, like
	the object's behavior on mouse clicks, etc...
	=================================================================================*/
	
	
	public class BaseObject
	{
		protected var gameObjectManager:GameObjectManager = null; //every object must have access to its gameObjectManager data
		protected var zOrder:int = 0; //specifies an "ordering" in the game, e.g. which objects should be drawn before others	
		protected var gameID:int = 0; //every object has a unique gameID
		
		//properties that have to do with otherPlayer objects mostly, but can also be applied to other game objects 
		protected var nameID:String = null; //name reserved for players to be identified; for other objects, keep as null
		protected var isActive:Boolean = false; //other player objects are not active unless turned active
		
		public function get ZOrder():int
		{
			return this.zOrder;
		}
		
		public function get GameID():int
		{
			return this.gameID;
		}
		
		public function set set_NameID(name:String):void
		{
			this.nameID = name;	
		}
		
		public function get NameID():String
		{
			return this.nameID;
		}
		
		public function set Active(flag:Boolean):void
		{
			this.isActive = flag;
		}
	
		public function get IsActive():Boolean
		{
			return this.isActive;
		}
		
		public function BaseObject(gameObjectManager:GameObjectManager, gameID:int, zOrder:int, name:String = null)
		{
			this.gameID = gameID;
			this.zOrder = zOrder;
			this.gameObjectManager = gameObjectManager;
			this.gameObjectManager.addBaseObject(this);
			this.nameID = name;
		}
		
		public function shutdown():void
		{
			this.gameObjectManager.removeBaseObject(this);
		}
		
		public function enterFrame(dt:Number):void
		{
		
		}
		
		public function click(event:MouseEvent, scrollX:int, scrollY:int):void
		{
		
		}
		
		public function mouseDown(event:MouseEvent, scrollX:int, scrollY:int):void
		{
		
		}
		
		public function mouseUp(event:MouseEvent, scrollX:int, scrollY:int):void
		{
		
		}
		
		public function mouseMove(event:MouseEvent, scrollX:int, scrollY:int):void
		{
		
		}
	
		public function keyDownHandle(event:KeyboardEvent, scrollX:int, scrollY:int):void
		{
		
		}

		public function draw(surface:BitmapData, scrollX:int, scrollY:int):void
		{
		
		}
		
		public function move(parameter:Point): void
		{
			
		}
		
		public function customFunction(...Parameters):void
		{
			/*useful for classes that want to utilize a function specific to that class that can be called upon by other 
			functions. If so, please document carefully, because this function differs for every object class*/
			
		}

	}
}
