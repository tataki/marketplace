package
{
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	public class ChatRoomManager
	{
		/**=======================================================================
		 * Class ChatRoomManager
		 * 
		 * Manages three major tasks about the chatroom: 
		 * 		Establishes connection with the server to get chatlist info
		 * 		Manages conversation and player array collections (display arrays)
		 * 		Passes information among users in the network
		 * ======================================================================*/
		
		//List of the body of the text during the conversation; stored by the person creating the chat and can be viewed (not accessed) by everyone
		[Bindable] public static var ChatText:ArrayCollection = new ArrayCollection();
		//List of users; dataProvider for the users field
		[Bindable] public static var ChatUsers:ArrayCollection = new ArrayCollection();
		//List of administrators
		private var Admins:ArrayCollection = null;
		
		//COMET variablesit
		private var listenTo:Array = null;
		private var ignoredUsers:Array = null;
		private var uniCastMessage:Array = null;
		private var manager:Comet2ManagerImpl = null;
		private var messageDispatcher:MessageDispatcher = null;
		
		private var DestChannel:String = null;
		private var DestChannel_Prefix:String = "Chat_";
		private var chatRoomOwner:String = null;
		private var player:Player = null;
		
		/**ChatRoomManager
		 * 
		 * The chatroom object defines the creation of several ArrayCollections, Arrays,
		 * and vars to communicate with the potential system.
		 * */
		public function ChatRoomManager(chatRoomOwner:String, messageDispatcher:MessageDispatcher, cometManager:Comet2ManagerImpl, player:Player)
		{
			this.Admins = new ArrayCollection();
			this.chatRoomOwner = chatRoomOwner;
			this.messageDispatcher = messageDispatcher;
			this.manager = cometManager;
			this.Admins.addItem(chatRoomOwner);
			this.player = player;
		}
		
		/**Function initializeConnection():void
		 * 
		 * Needed to initialize connection to Waitless to communicate; make sure that the chatRoomOwner 
		 * field is set and correct before calling this function.
		 * NB: the channel to listen for is set as the 
		 * */
		public function initializeConnection():void
		{
			this.manager = new Comet2ManagerImpl();
			if(!this.manager.hasEventListener(Comet2ManagerEvent.COMET_DATA_RECEIVED))
			{
				this.manager.addEventListener(Comet2ManagerEvent.COMET_DATA_RECEIVED, onMessageReceived);
			}
			
			if(!this.messageDispatcher.hasEventListener(ChatRoomEvent.CHAT_ROOM_START))
			{
				this.messageDispatcher.addEventListener(ChatRoomEvent.CHAT_ROOM_START, createChatRoom);
			}
			
			if(!this.messageDispatcher.hasEventListener(ChatRoomEvent.CHAT_ROOM_SPEAK))
			{
				this.messageDispatcher.addEventListener(ChatRoomEvent.CHAT_ROOM_SPEAK, onSpeak);
			}
			
			this.listenTo = new Array();
			this.listenTo.push(this.DestChannel_Prefix + this.chatRoomOwner); 
			this.ignoredUsers = new Array();
			this.ignoredUsers.push(this.chatRoomOwner);
			this.uniCastMessage = null;
			startListen();
		}
		
		/**stopListen
		 * Stops listening to all channels
		 **/
		public function stopListen():void 
		{
			manager.stopReceiving();
			Alert.show("Stopped listening");
		}
		
		protected function startListen():void 
		{
			manager.startReceiving(this.listenTo, this.ignoredUsers, this.uniCastMessage);
		}
		
		
		/*onSpeak broadcasts a message to all users in this chatroom channel*/
		protected function onSpeak(evt:ChatRoomEvent):void
		{
			var message:String = evt.information;
			sendMessage(this.DestChannel_Prefix + this.chatRoomOwner, this.player.NameID, message);
		}
		
		private function sendMessage(destChannel:String, senderID:String, dataToBeSent:Object, target:String = null):void 
		{
			manager.sendData(destChannel, senderID, dataToBeSent, target);
			//Alert.show("sending: [" + destChannel + " , " + senderID + " , " + dataToBeSent.toString() + " ]");
		}
		
		private function createChatRoom(evt:ChatRoomEvent):void
		{
			this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.LAUNCH_CHATBOX, this.player.GameID, null)); 
		}
		
		/*after a message is */
		private function onMessageSent():void
		{
			
			
			
		}
		
		/*adds the contents of the COMET message to the chatboard.*/
		private function onMessageReceived(msg:Comet2ManagerEvent):void
		{
			ChatText.addItem(msg.content.toString());
		}
		
		
	}
}