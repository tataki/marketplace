package
{
	import flash.events.Event;
	
	/**=============================================================
	 * CHATROOMEVENTS
	 * Defines string constants to be used as chatroom events. Also,
	 * models an event within the chatroom. Events take the form of a
	 * regular event with a senderName identifying the object sender
	 * and a string with the info contained in the event in string form.
	 * ===========================================================\==*/
	
	public class ChatRoomEvent extends Event
	{
		public static const CHAT_ROOM_ENTER:String = "Action_EnterChatRoom";
		public static const CHAT_ROOM_EXIT:String = "Action_ExitChatRoom";
		public static const CHAT_ROOM_SPEAK:String = "Action_SpeakChatRoom";
		public static const CHAT_ROOM_START:String = "Action_ChatRoomStart";
		public static const CHAT_ROOM_REQUEST:String = "Action_ChatRoomRequest";
		public static const CHAT_ROOM_PENDING:String = "Action_ChatRoomPending";
		public static const CHAT_ROOM_LIST_USERS:String = "Action_ChatRoomListUsers";
		
		public var information:String;
		public var senderName:String = null;
		
		public function get Sender_Name():String
		{
			return this.senderName;
		}
		
		public function get Information():*
		{
			return this.information;
		}
		
		/**ChatRoomEvent
		 * A ChatRoomEvent describes some event that is released when the game progresses to a ChatRoom state. Events
		 * are tracked by the senderName, usually the id of the object that dispatched the event, and some information
		 * that is associated with the event.
		 * */ 
		public function ChatRoomEvent(type:String, senderName:String, info:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.information = info;	
			this.senderName = senderName;
		}
		
		
	}
}