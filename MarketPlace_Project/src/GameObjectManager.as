package
{
	import flash.display.*;
	import flash.events.*;
	import flash.utils.*;
	
	import mx.collections.*;
	import mx.controls.Alert;
	import mx.core.*;
	
	/**===================================================================================
	GAMEOBJECTMANAGER
	 * This class takes care of managing the information about all objects in the game.
	 * This means that it manages insertion/deletion of objects, drawing/removal from 
	 * the screen. It thus needs to keep runnning lists of new objects that are 
	 * to be created and objects that are to be removed from the screen, because you
	 * don't want to be adding and removing at the same time to an old array or during a loop.		
	 * This approach allows the code to be more secure from error, in other words, 
	 * while sacrificing some space. 
	======================================================================================*/
	
	public class GameObjectManager
	{
		// a collection of the BaseObjects 
		public var baseObjects:ArrayCollection = new ArrayCollection();
		// a collection where new BaseObjects are placed, to avoid adding items 
		
		// to baseObjects ywhile in the baseObjects collection while it is being processed in a loop
		public var newBaseObjects:ArrayCollection = new ArrayCollection();
		// a collection where removed BaseObjects are placed, to avoid removing items 
		// to baseObjects while in the baseObjects collection while it is being processed in a loop
		public var removedBaseObjects:ArrayCollection = new ArrayCollection();
		
		
		
		public function GameObjectManager()
		{

		}
		
		public function shutdown():void
		{
			shutdownAll();
		}
		
		public function enterFrame(seconds:Number):void
		{
	    	removeDeletedBaseObjects();
	    	insertNewBaseObjects();
	 	    		    	
	    	// now allow objects to update themselves
			for each (var gameObject:BaseObject in baseObjects)
				gameObject.enterFrame(seconds);	
			
		}
		
		public function click(event:MouseEvent, scrollX:int, scrollY:int):void
		{
			for each (var gameObject:BaseObject in baseObjects)
				try
				{
				gameObject.click(event, scrollX, scrollY);
				}
				catch (myError:Error)
				{
					//This error only happens if the world is smaller than the screen and the player has clicked outside the world tiles
					Alert.show("You can't click there!"); 
				}
		}
		
		public function mouseDown(event:MouseEvent, scrollX:int, scrollY:int):void
		{
			for each (var gameObject:BaseObject in baseObjects)
				gameObject.mouseDown(event, scrollX, scrollY);
		}
		
		public function mouseUp(event:MouseEvent, scrollX:int, scrollY:int):void
		{
			for each (var gameObject:BaseObject in baseObjects)
				gameObject.mouseUp(event, scrollX, scrollY);
		}
		
		public function showRemoved():void
		{
			for each(var removedO:BaseObject in removedBaseObjects)
			{
				Alert.show(removedO.GameID.toString());
			}
		}
		
		public function mouseMove(event:MouseEvent, scrollX:int, scrollY:int):void
		{
			for each (var gameObject:BaseObject in baseObjects)
				gameObject.mouseMove(event, scrollX, scrollY);
		}

		/*--------------------------------------------------------------------------------
		
		public function keyDownHandle(event:KeyboardEvent, scrollX:int, scrollY:int):void
		{
			if(event.keyCode >= 37 && event.keyCode <= 40) 
			{
				for each (var gameObject:BaseObject in baseObjects)
				gameObject.keyDownHandle(event, scrollX, scrollY);
			}
		}
		
		--------------------------------------------------------------------------------*/
		
		public function drawObjects(surface:BitmapData, scrollX:int, scrollY:int):void
		{
			// draws the objects in order as they appear 
			for each (var baseObject:BaseObject in baseObjects)
				baseObject.draw(surface, scrollX, scrollY);
		}
				
		public function addBaseObject(baseObject:BaseObject):void
		{
			newBaseObjects.addItem(baseObject);
		}
		
		public function removeBaseObject(baseObject:BaseObject):void
		{
			removedBaseObjects.addItem(baseObject);
		}
		
		protected function shutdownAll():void
		{
			// don't dispose objects twice
			for each (var baseObject:BaseObject in baseObjects)
			{
				var found:Boolean = false;
				for each (var removedObject:BaseObject in removedBaseObjects)
				{
					if (removedObject == baseObject)
					{
						found = true;
						break;
					}
				}
				
				if (!found)
					baseObject.shutdown();
			}
		}
		
		protected function insertNewBaseObjects():void
		{
			for each (var baseObject:BaseObject in newBaseObjects)
			{
				for (var i:int = 0; i < baseObjects.length; ++i)
				{ //finds the correct place to add each new base object
					var otherBaseObject:BaseObject = baseObjects.getItemAt(i) as BaseObject;
					
					if (otherBaseObject.ZOrder > baseObject.ZOrder ||
						otherBaseObject.ZOrder == -1)
						break;
				}
				baseObjects.addItemAt(baseObject, i);
			}
			newBaseObjects.removeAll();
		}
		
		protected function removeDeletedBaseObjects():void
		{
			
			for each (var removedObject:BaseObject in removedBaseObjects)
			{
				var i:int = 0;
				for (i = 0; i < baseObjects.length; ++i)
				{
					if (baseObjects.getItemAt(i) == removedObject)
					{
						baseObjects.removeItemAt(i);
						break;
					}
				}
				
			}
			
			removedBaseObjects.removeAll();
		}
	}
}
