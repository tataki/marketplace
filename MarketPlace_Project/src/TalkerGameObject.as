package
{
	import flash.geom.Point;

	public class TalkerGameObject extends InteractionGameObject
	{
		protected var conversationID:int = 0;
		
		public function get ConversationID():int
		{
			return this.conversationID;
		}
		
		protected function get TalkActionString():String
		{
			return "Talk To " + itemName;
		}
		
		public function TalkerGameObject(world:World, messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, itemName:String, graphics:GraphicsResource, position:Point, gameID:int, conversationID:int, zOrder:int=0)
		{
			super(world, messageDispatcher, gameObjectManager, itemName, graphics, position, gameID, zOrder);
			this.conversationID = conversationID;
			this.messageDispatcher.addEventListener(GameProgressEvent.PLAYER_STOP_MOVE, playerStopMove);
			this.messageDispatcher.addEventListener(GameProgressEvent.TALK_TO, talkto);
		}
		
		protected function playerStopMove(event:GameProgressEvent):void
		{
			var player:Player = event.ActionData as Player;
			if (player != null && this.playerWithinActionDistance(player.Position))
			{
				this.messageDispatcher.dispatchEvent(
					new GameProgressEvent(
						GameProgressEvent.DISPLAY_ACTIONS, 
						this.gameID, 
						[{name: this.TalkActionString, event: GameProgressEvent.TALK_TO}]));
			}
		}
		
		protected function talkto(event:GameProgressEvent):void
		{
			if (event.ActionData == this.gameID)
			{
				this.messageDispatcher.dispatchEvent(
					new GameProgressEvent(
						GameProgressEvent.TALK, 
						this.gameID, 
						{conversationId: this.conversationID, name: this.itemName}));
			}
		}		
	}
}