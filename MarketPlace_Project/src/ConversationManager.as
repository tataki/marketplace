package
{
	import flash.utils.Dictionary;
	
	/**This class uses the Conversations class to manage the interplay of
	responses and texts that occur during a conversation. 
	This section should be modified to accomodate new conversation texts and 
	responses. 
	The only thing to be aware of is the pre-defined range of conversation ID's.
	Conversation IDs start at 5000 and end at 6000.*/
	
	public class ConversationManager
	{
		protected static var conversations:Dictionary = new Dictionary();
		
		public static function GetConversation(id:int):Conversation
		{
			return conversations[id];
		}
		
		public static function AddConversation(id:int, conversation:Conversation):void
		{
			conversations[id] = conversation;
		}
		
		public static function CreateConversations():void
		{
			var conversation:Conversation = new Conversation();
			conversation.addConversationText(
				1,
				new ConversationText(
					"Hi there traveller! Can you help me? I have lost a blue gem somewhere in the city. I can share some important information if you found it for me.",
					[1, 5]
				)
			)
			conversation.addConversationText(
				2,
				new ConversationText(
					"Oh thankyou for finding my gem! I know I promised some important information, but unfortunately the code for that hasn't be written yet!",
					[2]
				)
			)
			conversation.addConversationText(
				3,
				new ConversationText(
					"Do you want to continue with an endless conversation?",
					[3, 4]
				)
			)
			conversation.addConversationResponse(
				1,
				new ConversationResponse(
					"I'll see what I can do.",
					Conversation.END_CONVERSATION
				)
			);
			conversation.addConversationResponse(
				2,
				new ConversationResponse(
					"Thanks for nothing.",
					Conversation.END_CONVERSATION
				)
			);
			conversation.addConversationResponse(
				3,
				new ConversationResponse(
					"I sure do.",
					3
				)
			);
			conversation.addConversationResponse(
				4,
				new ConversationResponse(
					"No thanks.",
					Conversation.END_CONVERSATION
				)
			);
			conversation.addConversationResponse(
				5,
				new ConversationResponse(
					"I want to know more.",
					3
				)
			);
			
			conversation.addConversationEntryPoint(2, 5000);
			conversation.addConversationEntryPoint(1, Conversation.NO_CONDITION_ENTRY_POINT);
			
			/*Call to ConversationsManager to add the right conversation ID...*/
			AddConversation(5000, conversation);
		}

	}
}