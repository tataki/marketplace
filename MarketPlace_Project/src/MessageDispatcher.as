package
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;

	
	
	public class MessageDispatcher extends EventDispatcher
	{
		
		/**=====================================================================
		MESSENGERDISPATCHER CLASS
		A central class for the RPG game. The central object that receives
		all events and redispatches them. Every event save for COMET events
		are handled by this class. 
		The equivalent class that receives COMET events is the CometManagerImpl
		class, which takes care of sending/getting data from the server.
		========================================================================*/
		
		public function MessageDispatcher(target:IEventDispatcher=null)
		{
			super(target);
		}	
		
		//public function NetworkDispatcher(target:IEventDispatcher=null) {
			/*establish network connection, transmit data*/
			
		//}
	}
	
}
