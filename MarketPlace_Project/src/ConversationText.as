package
{
		/*Constructor class for a "text" construction. In the game, an object
		of this class is one piece of dialogue in the conversationManager class. 
		The object keeps an array of ints identifying the proper responses to 
		this dialogue. */
	public class ConversationText
	{
		protected var parentConversation:Conversation;
		protected var text:String = "";
		protected var responseIDs:Array = new Array();
		
		public function get ParentConversation():Conversation
		{
			return parentConversation;		
		}
		
		public function set ParentConversation(parentConversation:Conversation):void
		{
			this.parentConversation = parentConversation;		
		}
		
		public function get Text():String
		{
			return text;		
		}
		
		public function get ResponseIDs():Array
		{
			return responseIDs;		
		}
		
		public function ConversationText(text:String, responseIDs:Array)
		{			
			this.text = text;
			this.responseIDs = responseIDs;
		}

	}
}