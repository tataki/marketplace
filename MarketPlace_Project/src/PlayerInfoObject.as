package
{
	[Bindable] 
	public class PlayerInfoObject
	{
		/**===============================================
		 * Defines a single instance of a player's info
		 * to be displayed in the Info textbox in the main 
		 * application.
		 * ===============================================
		 */
		
		protected var userName:String = null;
		protected var Guild:String = null;
		protected var Money:String = null; 
		
		public function get UserName():String
		{
			return this.userName;
		}
		
		public function get Amount():String
		{
			return Money;
		}
		
		public function get Membership():String
		{
			return Guild;
		}
		
		public function PlayerInfoObject(userName:String, Money:String /*Guild:String*/)
		{
			this.userName = userName;
			this.Money = Money;
			//this.guild = guild;
		}
	}
}