package
{
	/**From IComet2Manager:
	 * * An event have 3 parameters:
	 * - [dest] String identifying the event channel you want to listen.
	 * - [sender] String corresponding to the sender ID
	 * - [data] Object containing the data you want to send. Data must be JSONable
	 * - [target] Optional parameter: specify the destination of the message
	 	**/
	public class Comet2ReceivedMessage
	{
		public var channel:String;
		
		public var sender:String;
		
		public var data:Object;
		
		public var target:String;
		
		/*useful for error-checking and for jsoning*/
		public function toString():String{
			return "[channel="+channel+", sender="+sender+", data="+data+", target="+target+"]";
		}

	}
}