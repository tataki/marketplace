package
{
	import flash.utils.Dictionary;
	
	/**This class holds an array of objects from the conversation text and
	conversation response classes. An object of this class may be thought of as 
	a container for one "conversation". This container is populated and managed
	by the ConversationsManager class.
	Changes to this class changes the nature of a conversation from being
	simply text-response relationships.*/
	
	public class Conversation
	{
		public static const END_CONVERSATION:int = -1;
		public static const NO_CONDITION_ENTRY_POINT:int = -1;
		protected var entryPointConversationTextIDs:Array = new Array();
		protected var entryPointProgressMarkerIDs:Array = new Array();
		protected var conversationText:Dictionary = new Dictionary();
		protected var conversationResponses:Dictionary = new Dictionary();
		
		public function Conversation()
		{
			
		}
		
		public function addConversationText(id:int, text:ConversationText):void
		{
			conversationText[id] = text;
			text.ParentConversation = this;
		}
		
		public function addConversationResponse(id:int, text:ConversationResponse):void
		{
			conversationResponses[id] = text;
			text.ParentConversation = this;
		}
		
		public function addConversationEntryPoint(id:int, progressMarkerID:int):void
		{
			entryPointConversationTextIDs.push(id);
			entryPointProgressMarkerIDs.push(progressMarkerID);
		}
		
		public function getConversationText(id:int):ConversationText
		{
			return conversationText[id]; 
		}
		
		public function getConversationResponse(id:int):ConversationResponse
		{
			return conversationResponses[id]; 
		}
		
		public function getConversationEntryPoint():ConversationText
		{
			for (var i:int = 0; i < entryPointConversationTextIDs.length; ++i)
			{
				var conversationTextId:int = entryPointConversationTextIDs[i];
				var progressID:int = entryPointProgressMarkerIDs[i];
				
				if (progressID == NO_CONDITION_ENTRY_POINT ||
					GameProgressEventManager.GameProgressMarkerCompleted(progressID))
				{
					return getConversationText(conversationTextId);
				}
			}
			
			return null;
		}

	}
}