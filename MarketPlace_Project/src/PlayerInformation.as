	package
	{
		import com.serialization.json.*;
		
		import flash.events.*;
		import flash.net.URLLoader;
		import flash.net.URLRequest;
		import flash.net.URLRequestMethod;
		
		import mx.collections.ArrayCollection;
		import mx.controls.Alert;
		public class PlayerInformation
		{
			
			/**============================================================================
			 * Uses items from PDManager (abbrev) to populate the database with the current 
			 * player's Name, Guild_Membership, and money amount.
			 * The contents of this class are displayed in the Info panel of main.mxml. 
			 * ============================================================================*/
			
			private var userName:String = null;
			private var Guild_Membership:String = null;
			private var playerMoney:String = null;
			
			private var urlReq:URLRequest = null;
			private var urlLdr:URLLoader = null;
			
			public function PlayerInformation(userName:String)
			{
				this.userName = userName;
				ObtainPlayerInformation();
				
			}
			
			private function ObtainPlayerInformation():void
			{
				urlReq = new URLRequest(PaperochDatabaseManager.WAITLESS_URL);
				urlReq.contentType = "application/json";
				urlReq.method = URLRequestMethod.POST;
				urlReq.data = (PaperochDatabaseManager.SELECT_START_COMMAND + " value " + PaperochDatabaseManager.DATABASE_CALL_COMMAND 
					+ "where name='" + this.userName + "'" + PaperochDatabaseManager.SQL_ENDING);
				
				/* Initialize the URLLoader object, assign the various event listeners, and load the specified URLRequest object. */
				urlLdr = new URLLoader();
				urlLdr.addEventListener(Event.COMPLETE, CompleteHandler);
				urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ErrorHandler);
				urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ErrorHandler);
				urlLdr.load(urlReq);
			}
			
			public function SetPlayerInformation():void
			{
				GameProgressEventManager.AllPlayerInformation.addItem( new PlayerInfoObject(this.userName, this.playerMoney) );
			}
			
			/*Here we complete the event by storing the resulting data in a GameProgressEvent object and having the main application receive it*/
			private function CompleteHandler(evt:Event):void 
			{
				if(evt.type == Event.COMPLETE) {
					var loader:URLLoader = URLLoader(evt.target);
					var objectData:Object = JSON.decode(loader.data);
					this.playerMoney = objectData.toString();
					SetPlayerInformation();
				}
			}
			
			private function ErrorHandler(errorEvent:Event):void
			{
				Alert.show("Error getting player info");
			}
			
		}
	}