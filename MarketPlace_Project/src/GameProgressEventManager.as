package
{
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	/**==================================================================================
	* Managing quest tasks that the user must fulfill. Once these actions are completed,
	* an event can be fired that for example unlocks something or shows something that was
	* previously hidden.
	*==================================================================================*/
	
	public class GameProgressEventManager
	{
		/*holds those quests that the player has yet to complete.*/
		[Bindable] protected static var pendingVisibleProgressEvents:ArrayCollection = new ArrayCollection();
		
		/*holds those quests that the player has completed.*/
		[Bindable] protected static var completedVisibleProgressEvents:ArrayCollection = new ArrayCollection();
		
		[Bindable] protected static var playerInformation:ArrayCollection = new ArrayCollection();
		
		
		/*holds those quests that is not visible to the player.*/
		protected static var hiddenProgressEvents:ArrayCollection = new ArrayCollection();
		
		public static function get AllPlayerInformation():ArrayCollection
		{
			return playerInformation;
		}

		public static function get PendingVisibleProgressEvents():ArrayCollection
		{
			return pendingVisibleProgressEvents;
		}
		
		public static function get CompletedVisibleProgressEvents():ArrayCollection
		{
			return completedVisibleProgressEvents;
		}
		
		public static function get HiddenProgressEvents():ArrayCollection
		{
			return hiddenProgressEvents;
		}
		
		public static function clearPlayerInfo():void
		{
			playerInformation.removeAll();
		}
		
		/*obtains player information from WaitlessServer; relies on the PlayerInformation class*/
		public static function ObtainPlayerInfo(userName:String):void
		{
			var info:PlayerInformation = new PlayerInformation(userName);
			//return info.AllPlayerInformation;
		}
		
		/*Dispatches events to indicate certain events have been completed.
		When the progress marker is completed, the event is dispatched to be handled
		presumably by another function within GameProgressEventManager.*/
		public static function CreateGameProgressEventManager(messageDispatcher:MessageDispatcher):void
		{
				
			/*new GameProgressMarker(
				messageDispatcher,
				"Buy a PaPeRo",
				"You must buy PaPeRo's from the Village Store Clerk.",
				5000,
				null,
				true,
				3,
				GameProgressEvent.BUY_PAPERO);
				
			new GameProgressMarker(
				messageDispatcher,
				"Sell a PaPeRo",
				"You may sell a PaPeRo to a passerby.",
				5000,
				null,
				true,
				3,
				GameProgressEvent.SELL_PAPERO);*/
		}
		
		/*bool GameProgressMarketCompleted
		* Returns true if the specified gameProgressMarkerID is within the completedEvents.*/
		public static function GameProgressMarkerCompleted(gameProgressMarkerID:int):Boolean
		{
			for each (var marker:GameProgressMarker in completedVisibleProgressEvents)
			{
				if (marker.MyID == gameProgressMarkerID)
					return true;
			}	
			
			return false;	
		}
	}
}
