package
{
	import flash.errors.MemoryError;
	import flash.geom.Point;
	
	/**===========================================================================================
		This class handles the creation of objects in the game. 
		This class is called upon by the ResourcesManager class when it comes time
		to draw characters and objects onto the screen.
		This class needs to be updated every time the user enters a new kind of object.
		By "new kind" I mean an object whose property differs in any way from previously-created
		objects. I acknowledge that this is kind of a pain. It would be great if this process
		could be automated.
		
		If you wind up having an object that falls outside of "InteractableObject" or 
		"StructuralObject," a new function should be written that indicates semantically
		the nature of the new kind of object.
	==============================================================================================*/
	
	public class LevelObjectManager
	{
		/**type defines the general type of object; ID is unique to each object*/
		public static function CreateLevelObject(messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, world:World, position:Point, type:int, id:int):GameObject
		{
			if (type >= 0 && type <= 9999)	//check in case a number is mistakenly typed in		
				return CreateInteractableObject(messageDispatcher, gameObjectManager, world, position, type, id);
			
			
			return null;
		}
		
		/*Interactable objects define broadly any kind of individual objects that are buyable, able to be picked up, or
		has any other property that can be modified by the user.*/
		protected static function CreateInteractableObject(messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, world:World, position:Point, type:int, id:int):GameObject
		{
			if(type == 0)
				return new TalkerGameObject(world, messageDispatcher, gameObjectManager, "Blue Gem", new GraphicsResource("gem_blue"), position, 1, 5000);
				
			//if(type == 1001) 
				//return new TalkerGameObject(world, messageDispatcher, gameObjectManager, "Girl1NPC", new GraphicsResource("girl1NPC"), position, 3, 5000);
				
			if(type == 1000)
				return new AdvertiserGameObject(world, messageDispatcher, gameObjectManager, "Girl1NPC", new GraphicsResource("girl1NPC"), position, 4);	
			
			
			return null;
		}
		
		/**These are structural objects that transport the player from locations. They're structural because they
		aren't attributed to any single object, but parts of bigger structures. Thus, to interact with a part of a building,
		one would add to the cases in this function.*/
		public static function CreateStructuralObject(messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, world:World, destinationWorld:int, position:Point, type:int, id:int):GameObject
		{
			if(type >= 10000 && type <= 99999) 
				return StructuralObject(world, destinationWorld, messageDispatcher, gameObjectManager, position, type, id);
			return null;
		}
		
		//protected function to hide modifiable info
		protected static function StructuralObject(world:World, destinationWorld:int, messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, position:Point, type:int, id:int):GameObject
		{
			if(type >= 10000 && type <= 10999) 
				return new EntranceGameObject(world, destinationWorld, messageDispatcher, gameObjectManager, position, type, id);
			return null;
		}
		
	
	}
}
