package
{
	
	import mx.logging.Log;
	
	public class TextPadLoggerConfig
	{
		// DEFAULT LEVEL
		public var defaultLevel:int = LogLevel.DEBUG;
		
		// AUTO SCROLL DEFAULT
		public var autoScroll:Boolean = true;
		
		private var levels:Object = {
			"ServerAccessor.request.HTTPHandler": INFO,
			"Common.Comet": DEBUG,
			"Common.CoEditor": DEBUG,
			"Common.Controller": DEBUG,
			"Common.CoEditor.RefreshHandler": INFO,
			"Discussion": INFO,
			"Login": INFO
		};
		
		// USE THIS FUNCTION TO GET LOG LEVEL
		public function getLevel(name:String):int {
			if(levels.hasOwnProperty(name)){
				return levels[name];
			}
			return defaultLevel;
		}
		
		private var ERROR:int = LogLevel.ERROR;
		private var INFO:int = LogLevel.INFO;
		private var DEBUG:int = LogLevel.DEBUG;
		private var NONE:int = LogLevel.NONE;
		

	}
}