package com.electrotank.astar 
{
	/**
	 * A tile that will work with the A* search needs to implment INode. It can extend this class for convenience, which already implements INode.
	 */
	public class Node implements INode
	{
		private var heuristic:Number;
		private var neighbors:Array;
		private var col:int;
		private var row:int;
		private var nodeId:String;
		private var passable:Boolean;
		
		public function Node(row:int, col:int, passable:Boolean) 		
		{			
			this.row = row;
			this.col = col;
			this.passable = passable;
		}
		public function setNodeId(nodeId:String):void {
			this.nodeId = nodeId;
		}
		public function getNodeId():String {
			return nodeId;
		}
		public function setPassable(p:Boolean):void {
			this.passable = p;
		}
		public function getPassable():Boolean {
			return passable;
		}
		public function setCol(num:int):void {
			col = num;
		}
		public function getCol():int {
			return col;
		}
		public function setRow(num:int):void {
			row = num;
		}
		public function getRow():int {
			return row;
		}
		public function setNeighbors(arr:Array):void {
			neighbors = arr;
		}
		public function getNeighbors():Array {
			return neighbors;
		}
		public function setHeuristic(h:Number):void {
			heuristic = h;
		}
		public function getHeuristic():Number {
			return heuristic;
		}
		
	}
	
}
