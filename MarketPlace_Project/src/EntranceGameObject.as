package
{
	import flash.display.*;
	import flash.geom.Point;
	
	/**Class MenuGameObject
	*===================================================================================================
	* Generic class that takes you from one specified world to another, as long as you are in front of 
	* the entrance/exit. 
	 =====================================================================================================*/
	
	public class EntranceGameObject extends InteractionGameObject
	{
		//TODO: Change DestinationWorld if necessary
		private var DestinationWorld:int=1; //by default, "bottom" world is set
		private var type:int;
		
		/*for displaying the appropriate action on the button*/
		public function get ActionVerb():String
		{
			if(this.type % 2 == 0) return "Enter"; 
			else return "Exit"; 
		}
		
		/*Destination returns the proper world for the main application to load.*/
		public function get Destination():int
		{
			return DestinationWorld;
		}
		
		public function EntranceGameObject(world:World, DestinationWorld:int, messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, position:Point, type:int, gameID:int, zOrder:int = 0) 
		{
			super(world, messageDispatcher, gameObjectManager, "", null, position, gameID, zOrder);		
			this.messageDispatcher.addEventListener(GameProgressEvent.PLAYER_STOP_MOVE, playerStopMove);
			this.DestinationWorld= DestinationWorld;
			this.type=type;
		}
		
		/*player needs to stop in front of the door before any actions can be displayed*/
		protected function playerStopMove(event:GameProgressEvent):void
		{
			var player:Player = event.ActionData as Player;
			if (player != null && this.playerWithinActionDistance(player.Position))
			{
				if(this.ActionVerb=="Enter") 
				{
					this.messageDispatcher.dispatchEvent(
						new GameProgressEvent(
							GameProgressEvent.IN_FRONT_OF_PORTAL,
							this.Destination, 
							[{name: this.ActionVerb, event: GameProgressEvent.ENTER_WORLD}]));
				}
				else
				{
					this.messageDispatcher.dispatchEvent(
						new GameProgressEvent(
							GameProgressEvent.IN_FRONT_OF_PORTAL,
							this.Destination, 
							[{name: this.ActionVerb, event: GameProgressEvent.EXIT_WORLD}]));
				}
			}
		}
		
		override public function draw(surface:BitmapData, scrollX:int, scrollY:int):void
		{
			/*overrides drawing function that seeks to draw something onto the screen; we don't want doors being 
			painted over or moving*/
		}

	}
}