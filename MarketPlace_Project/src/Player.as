package
{
	import com.electrotank.astar.INode;
	import com.electrotank.astar.SearchResults;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	/**===============================================================================================
	 * Player Class
	 * Defines movement for active players.
	 * Player objects have special attributes such as an astar object (to find the shortest path
	 * in the map), access to the current world's data, and access to the messengerDispatcher's data
	 * Changes should be made to this class in order to change/include properties, etc... 
	 * about the player (like health and information displayed).
	 * 
	 * NOTE: This class contains some code for keyboard functions that were not implemented
	 * due to a graphics offset. It's pretty easy to correct, but I abandoned it due to reasons cited
	 * (see below for details). I think that would be an interesting alternative to mouses
	 * as it allows the player to "explore" more of the world on their own without having 
	 * the A-star algorithm simply tell them how to get there.
	 * ==============================================================================================*/
	
	public class Player extends GameObject
	{
		protected static const MOVE_TIME:Number = 0.4;
		protected var messageDispatcher:MessageDispatcher = null;
		protected var world:World = null;
		protected var path:Array = null;
		protected var nextTarget:int = -1;
		protected var otherPlayerManager:OtherPlayerManager = null;
		protected var manager:Comet2ManagerImpl = null;
		protected var currentWorld:int = -1;
		
		//COMET variables (for broadcasting your location)
		//defines the string name of common "channel" that a group of users listen to; needs to add the name of the currentWorld to complete the string
		private var destChannel:String = "MOVE_"; 
		//your ID
		private var senderID:String; 
		//must be of the form 
		private var dataToBeSent:Object = null; 
		//optional parameter used when targeting a specific user; generally null
		private var target:String = null; 
		private var chatActive:Boolean = true;
		
		/**Player Constructor
		 * A player object keeps track of properties of the main player, and has access to most of the games information, including
		 * a list of other objects within the world. 
		 * 
		 * Most series of multi-player actions start in this class, including movement and chatting systems.
		 * */
		public function Player(messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, position:Point, world:World, currentWorld:int)
		{
			super(gameObjectManager, new GraphicsResource("player"), position, GameObjectIDs.PLAYER)
			this.world = world;
			this.messageDispatcher = messageDispatcher;	
			this.manager = new Comet2ManagerImpl();
			manager.addEventListener(Comet2ManagerEvent.COMET_REQUEST_SENT, requestSent);
			this.currentWorld = currentWorld;
		}
		
		public function get playerPosition():Point
		{
			return this.position;
		}
		
		public function get DestChannel():String
		{
			return this.destChannel;
		}
		
		public function get CurrentWorld():int
		{
			return this.currentWorld;
		}
		
		public function set setTargetUser(userName:String):void
		{
			this.target = userName;
		}
		
		public function get ChatActive():Boolean
		{
			return this.chatActive;
		}
		
		private function requestSent(event:Comet2ManagerEvent):void 
		{
			//Alert.show("Request Sent");
		}
		
		/*sends a message containing userName, position, and guild information, about the player.*/
		private function sendMessage(destChannel:String, senderID:String, dataToBeSent:Object, target:String = null):void 
		{
			manager.sendData(destChannel, senderID, dataToBeSent, target);
			//Alert.show("sending: [" + destChannel + " , " + senderID + " , " + dataToBeSent.toString() + " ]");
		}
		
		/**FUNCTION CLICK
		 * overrides the empty click function from GameObjectManager. The world tiles are first searched for an optimal path using the A-star algorithm; 
		 * when a path is found, the player is tweened toward the target. A signal is then sent to the appropriate channel to update the position of this
		 * player in other players' worlds.
		 *
		 *  */ 
		public override function click(event:MouseEvent, scrollX:int, scrollY:int):void
		{
			TweenLite.killTweensOf(this.position);
			
			var clickTile:Point = this.world.pixelToTile(new Point(event.localX + scrollX, event.localY + scrollY));			
			var currentTile:Point = this.world.pixelToTile(this.position);
			var result:SearchResults = world.search(currentTile.x, currentTile.y, clickTile.x, clickTile.y);
			
			if (result.getIsSuccess())
			{
				
				this.path = result.getPath().getNodes();
				this.nextTarget = 0;
				moveToNextPoint();				
				this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PLAYER_START_MOVE, GameObjectIDs.PLAYER, this));
				
				
				//we must send the target square to other players in the world
				var dataToBeSent:Object = { Name: "" , WorldNumber: "" , PointX: "", PointY: "", Guild: "" };
				dataToBeSent.Name = this.NameID;
				dataToBeSent.WorldNumber = this.currentWorld;
				dataToBeSent.PointX = clickTile.x * this.world.TileWidth;
				dataToBeSent.PointY = clickTile.y * this.world.TileHeight;
				sendMessage(this.destChannel + this.CurrentWorld.toString(), this.NameID, dataToBeSent);
				//checkNearbyPlayers();
			} 
			else
			{
				this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.CLICKED_COLLISION_TILE, GameObjectIDs.PLAYER, this));
			}
		}
		
		/*checkNearbyPlayers
		* Function checks if there are any OtherPlayer objects nearby and, if so, creates an array of
		* player names linked to events to be displayed. Used in chatting system implementation.
		*/
		protected function checkNearbyPlayers():void
		{
			var nearbyPlayerNames:Array = new Array(); //an array of objects with properties name attributed to the event string within_chatting_distance 
			
			for each(var gameObject:BaseObject in this.gameObjectManager.baseObjects)
			{
				if(gameObject.IsActive) 
				{ //we know that if it is active, it must be some other player object
					var OPlayer:OtherPlayer = gameObject as OtherPlayer;
					if(withinTalkingDistance(OPlayer.Position)) 
					{
						var playerEntry:Object = { name: "", event: ""};
						playerEntry.name = "Talk to: " + OPlayer.NameID;
						playerEntry.event = ChatRoomEvent.CHAT_ROOM_REQUEST;
						nearbyPlayerNames.push(playerEntry);
					}
				}
			}	
	
		}
		
		/*small function that checks for whether a point is within one square of the main player.*/
		protected function withinTalkingDistance(point2:Point):Boolean
		{
			var player1Tile:Point = world.pixelToTile(this.position);
			var player2Tile:Point = world.pixelToTile(point2);
			var xDist:int = Math.abs(player1Tile.x - player2Tile.x);
			var yDist:int = Math.abs(player1Tile.y - player2Tile.y);
			
			return xDist <= 1 && yDist <= 1;
		}
		
		//algorithm that tween-moves the character from one node to the next until the target node is reached
		protected function moveToNextPoint():void
		{
			++this.nextTarget;
			if (path != null && nextTarget < path.length)
			{
				var destinationNode:INode = path[nextTarget] as INode;
				var destinationXPoint:int = destinationNode.getCol() * this.world.TileWidth;
				var destinationYPoint:int = destinationNode.getRow() * this.world.TileHeight;
				
				/*tail-end recursive implementation of move function*/
				TweenLite.to(
					this.position, 
					MOVE_TIME, 
					{
						x:destinationXPoint, 
						y:destinationYPoint, 
						onComplete:moveToNextPoint, 
						ease:Linear.easeNone
					}
				);
			}
			else
			{
				path = null;
				nextTarget = 0;	
				this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PLAYER_STOP_MOVE, GameObjectIDs.PLAYER, this));
				
			}
		}		
		
		/******************************* Code for Key Controls *************************************************
		 * There was some weird rounding error that I couldn't overcome, so I decided against implementing it this way.
		 * Basically what happens is that when you press multiple events, integers are added in such a way such that
		 * this approach can have you moving between two tiles. It's a fixable error; one way would be simply to add
		 * on tiles or to disable the key listeners until the character has stopped moving. 
		 * 
		 * Besides, the focus is not quest-centered but ease of navigation. If this were a quest-based game then
		 * this section would be appropriate. Otherwise, it's better to implement this using mouse controls and 
		 * least-cost paths. 
		 
		 public override function keyDownHandle(event:KeyboardEvent, scrollX:int, scrollY:int):void
		 {
		 TweenLite.killTweensOf(this.position);
		 
		 
		 var currentTile:Point = this.world.pixelToTile(this.position);
		 //var newTile:Point = this.world.pixelToTile(new Point(currentTile.x-1/(this.world.TileWidth),
		 //						           currentTile.y-1/(this.world.TileHeight));
		 
		 
		 
		 
		 
		 if(event.keyCode == 37) {			
		 if(currentTile.x - 1/(this.world >=0) {
		 arriveXPoint = this.position.x - this.world.TileWidth;
		 arriveYPoint = this.position.y;
		 }
		 }
		 
		 if(event.keyCode == 38) {				
		 if(this.position.y - this.world.TileHeight >=0) {
		 arriveXPoint = this.position.x;
		 arriveYPoint = this.position.y - this.world.TileHeight;
		 }
		 }
		 
		 if(event.keyCode == 39) {			
		 if(this.position.x + this.world.TileWidth <= this.world.GroundLayerSizeX) {
		 arriveXPoint = this.position.x + this.world.TileWidth;
		 arriveYPoint = this.position.y;
		 }
		 }
		 
		 if(event.keyCode == 40) {			
		 if(this.position.x + this.world.TileHeight <= this.world.GroundLayerSizeY) {
		 arriveXPoint = this.position.x;
		 arriveYPoint = this.position.y + this.world.TileHeight;
		 }
		 }
		 
		 TweenLite.to(
		 this.position, 
		 MOVE_TIME, 
		 {
		 x:arriveXPoint, 
		 y:arriveYPoint, 
		 ease:Linear.easeNone
		 }
		 );
		 
		 messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PLAYER_START_MOVE, GameObjectIDs.PLAYER, this));
		 
		 }
		 *********************************************************************************************************/	
		
		
		
	}
}
