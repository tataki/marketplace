package 
{
	import flash.display.*;
	import flash.geom.*;
		
	public class GraphicsResource
	{
		protected var resourceName:String = "";
		protected var frames:int = 1;
		protected var fps:Number = 0;
		protected var drawRect:Rectangle = null;
		
		public function get ResourceName():String
		{
			return resourceName;	
		}
		
		public function get Frames():int
		{
			return frames;	
		}
		
		public function get FPS():Number
		{
			return fps;	
		}
		
		public function get DrawRect():Rectangle
		{
			return drawRect;	
		}
		
		public function GraphicsResource(resourceName:String, frames:int = 1, fps:Number = 0, drawRect:Rectangle = null)
		{
			this.resourceName = resourceName;
			this.frames = frames;
			this.fps = fps;
			if (drawRect == null)
				this.drawRect = ResourceManager.GetResource(this.resourceName).rect;
			else
				this.drawRect = drawRect;
		}
	}
}