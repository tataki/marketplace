package
{
	import flash.geom.Point;

	/**=========================================================================================
	* INTERACTION_GAME_OBJECT CLASS
	*  Class that handles characteristics of all kinds of objects. This class
	* should be extended if at any point you would like to create new kinds of 
	* objects that responded to custom events by the users. To do so you will have
	* to modify the GameProgressEvent class to detect these events. 
	* Please see the documentation for how to do so in the src folder.
	
	* Modifications to this class should pertain to the game geometry
	* and indicate when these events take place
	* e.g. when a player is moving, when a player is within a certain
	* distance of the gameObject, etc...
	* To understand how the geometry works, it's easiest to refer to 
	* functions already written. To modify graphics drawing method, one would have to go to
	* the world class and look the subsequent classes that are called upon.
	* ========================================================================================*/
	public class InteractionGameObject extends GameObject
	{
		protected var messageDispatcher:MessageDispatcher = null;
		protected var world:World = null; //class needs access to this data for calculating geometry
		protected var itemName:String = ""; 
		
		public function get ItemName():String
		{
			return itemName;
		}
		
		public function InteractionGameObject(world:World, messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, itemName:String, graphics:GraphicsResource, position:Point, gameID:int, zOrder:int=0)
		{
			super(gameObjectManager, graphics, position, gameID, zOrder);
			this.world = world;	
			this.itemName = itemName;
			this.messageDispatcher = messageDispatcher;		
		}
		
		/*The following functions handle calculations of player proximity in order that the appropriate events
		* can be executed at the right times.
		* These events work in collaboration with the class that extends InteractionGameObjects:
		*                   -PickUpGameObject.as
		*					-MenuGameObject.as	
		* 					-TalkerGameObject.as
		*					And others that have the format {Description}GameObject.as	
		*/
		
		/*Boolean playerWithinActionDistance
		* Returns true if the player's Point is within one square of the object*/
		protected function playerWithinActionDistance(playerPoint:Point):Boolean
		{
			var playerTile:Point = world.pixelToTile(playerPoint);
			var localTile:Point = world.pixelToTile(this.position);
			var xDist:int = Math.abs(playerTile.x - localTile.x);
			var yDist:int = Math.abs(playerTile.y - localTile.y);
			
			return xDist <= 1 && yDist <= 1;
		}		
		
		/*Boolean playerFacingCounter
		* Returns true if the player's Point location is exactly two square in front of the object's square
		* (the player and object are separated by a counter.
		*/
		protected function playerFacingCounter(playerPoint:Point):Boolean
		{
			var playerTile:Point = world.pixelToTile(playerPoint);
			var localTile:Point = world.pixelToTile(this.position);
			var xDist:int = Math.abs(playerTile.x - localTile.x);
			var yDist:int = Math.abs(playerTile.y - localTile.y);
			
			return (xDist == 0 && yDist <= 2) || (yDist==0 && xDist <=2);
		}
	}
}
