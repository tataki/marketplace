package
{
	public class GameObjectIDs
	{
		public static const PLAYER:int = 99999;
		public static const NPC:int = 100000;
		public static const OTHERPLAYER:int = 100001;
		
		/*Here we define some unmodifiable constants for objects. 
		This class should be the place for important objects in the game, or anything
		worth naming.*/
	}
}