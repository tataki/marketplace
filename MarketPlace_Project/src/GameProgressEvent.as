package
{
	import flash.events.Event;
	
	/** GameProgressEvent Class
	 * This class is a repository for the string constants for the various events that will be dispatched as actions are 
		implemented. These strings will be used to create and dispatch events in one line, e.g.
		
			" dispatchEvent(new Event(PROGRESSMARKER_COMPLETED, ...))"
			
		This is often a neater solution than declaring all these variables as type eventObjects, although that
		is a viable option in some cases. 
		
		Because this class is used heavily for creating and detecting new game events,
		it is important to mark constants and functions as public.
	*/

	public class GameProgressEvent extends Event
	{
		/*------------Actions for movement, Game Progress, Conversations...*/
		public static const PROGRESSMARKER_COMPLETED:String = "Action_ProgressMarker_Completed";
		public static const PICKUP_OBJECT:String = "Action_PickupObject";
		public static const OBJECT_PICKED_UP:String = "Action_ObjectPickedUp";
		
		public static const TALK:String = "Action_Talk";
		public static const TALK_RESPONSE:String = "Action_TalkResponse";
		public static const TALK_TO:String = "Action_TalkTo";
		public static const END_TALK:String = "Action_EndTalk";
		
		public static const PLAYER_START_MOVE:String = "Action_PlayerStartMove";
		public static const PLAYER_STOP_MOVE:String = "Action_PlayerStopMove";
		public static const DISPLAY_ACTIONS:String = "Action_DisplayActions";
		
		public static const IN_FRONT_OF_PORTAL:String = "Action_InFrontOfPortal"; //Entrance means either entrance or exit; event used to signal in front either entrance or exit
		public static const ENTER_WORLD:String = "Action_EnterWorld";
		public static const EXIT_WORLD:String = "Action_ExitWorld"; 
		public static const ACTIVATE_TRANSPORT:String = "Action_ActivateTransport";
		
		public static const WITHIN_CHATTING_DISTANCE:String = "Action_WithinChattingDistance";
		public static const CLICKED_COLLISION_TILE:String = "Action_ClickedCollisionTile";
		public static const LAUNCH_CHATBOX:String = "Action_LaunchChatBox"; 
		
		/*------------PAPERO actions; previous implementation of this game; no longer used------------------------------------
		public static const INPUT_SELLER:String = "Action_InputSeller";
		public static const VIEW_PRICES:String = "Action_ViewPrices";
		public static const BUY_PAPERO:String = "Action_BuyPaPeRo";
		public static const SELL_PAPERO:String = "Action_SellPaPeRo";
		public static const LOAD_PAPERO:String = "Action_LoadPaPeRo";
		public static const PRICE_SET:String = "Action_PriceSet";
		public static const BOUGHT_PAPERO:String = "Action_BoughtPaPeRo";
		public static const SUBTRACT_COST:String = "Action_SubtractCost";
		public static const ADD_COST:String = "Action_AddCost";
		public static const USER_IDENTIFIED:String = "Action_UserIdentified";
		*/
		
		public static const LOOK_UP_DBASE_NAME:String = "Action_LookUpDBaseName";
		public static const LOOKED_UP_DBASE_NAME:String = "Action_LookedUpDBaseName";
		
		/*Properties of Event: SenderID is the object number that dispatches the event.
		NOTE: The EntranceGameObject class which utilizes this function defines senderID
		as the destinationWorld property. This is so that the ActionData field for the
		main class's displayActions function can retain appropriate information.*/
		protected var senderID:int = -1;
		protected var actionData:* = null;
		
		public function get SenderID():int
		{
			return senderID;
		}
		
		/*This is a rather ambiguous property at first, but for a reason.
		Most of the classes that extend InteractionGameObject define ActionData with
		an entry or multiple entries where each entry has the following syntax:
		[{name: myActionName, event: myTriggeredEvent}]
		where myActionName is the string name that appears on an action button,
		and myTriggeredEvent is an event that is likely handled by GameProgressEventManager.
		However, because this type is simply listed as a star, it can be basically 
		any information that is passable onto a display in the screen, or other kind
		of data associated with this event (think of it as extra parameters that cannot
		be passed into an addEventListener function.*/  
		public function get ActionData():*
		{
			return actionData;
		}
		
		public function GameProgressEvent(type:String, senderID:int, actionData:*=null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			this.senderID = senderID;
			this.actionData = actionData;
		}
		
	}
}
