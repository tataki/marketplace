package
{
	import mx.events.CollectionEvent;
	
	/**======================================================================================================================================
	* CLASS GAMEPROGRESSMARKER
	* This class implements a quest by defining certain markers for items when they are picked up. When a marker is 
	* constructed, parameters are passed which attribute an object of this class to a certain item. 
	* When completed (picked up, finished, transferred, etc), an event is dispatched that tells the program the event has been completed
	*====================================================================================================================================*/
	
	[Bindable]
	public class GameProgressMarker
	{
		protected var title:String = "";
		protected var detail:String = "";
		protected var myId:int = -1;
		protected var myActionData:* = null;
		protected var visible:Boolean = false;
		protected var senderId:int = -1;
		protected var actionType:String = "";
		protected var completed:Boolean = false;
		protected var messageDispatcher:MessageDispatcher = null;
		
		public function get MyID():int
		{
			return myId;
		}
		
		public function get Title():String
		{
			return title;
		}

		public function get Detail():String
		{
			return detail;
		}
		
		public function get Completed():Boolean
		{
			return completed;
		}
		
		public function GameProgressMarker(messageDispatcher:MessageDispatcher, title:String, detail:String, myId:int, myActionData:*, visible:Boolean, senderId:int, actionType:String)
		{
			this.messageDispatcher = messageDispatcher;
			this.title = title;
			this.detail = detail;
			this.myId = myId;
			this.visible = visible;
			this.senderId = senderId;
			this.actionType = actionType;
			this.myActionData = myActionData;
			
			if (this.visible)
				GameProgressEventManager.PendingVisibleProgressEvents.addItem(this);
			else
				GameProgressEventManager.HiddenProgressEvents.addItem(this);
				
			this.messageDispatcher.addEventListener(this.actionType, progressEvent);				
		}
		
		/*called when a completion event is dispatched by a progress marker.*/ 
		protected function progressEvent(event:GameProgressEvent):void
		{
			if (event.SenderID == this.senderId &&
				event.type == this.actionType)
			{
				this.completed = true;
				
				if (this.visible)
				{
					GameProgressEventManager.PendingVisibleProgressEvents.removeItemAt(
						GameProgressEventManager.PendingVisibleProgressEvents.getItemIndex(this));
					GameProgressEventManager.CompletedVisibleProgressEvents.addItem(this);
				}
				
				this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PROGRESSMARKER_COMPLETED, this.myId, this.myActionData));		
			}
		}
		
		

	}
}
