// ActionScript file
/** MAIN_SCRIPT *****************************************************************************************************************
 *  Contains the source code necessary to run the game. 
 *  Here are the important states referenced by this file that are located in main.mxml. I'm listing
 *  them for viewer's conveniece of reference:
 * 			State id=Register (used to login).
 * 			State id=Talk (used to have the player be able to talk to someone
 * 			State id=Info (used to display a custom component that displays dialogue and player info
 * 			State id=Game (the state used to manage game-playing aspects of Flash)
 * For the most part, the Info and Talk states are taken care of. Changes that are made would mainly be done in the Game states.
 * States do not represent different worlds. Instead, all the resources needed to draw all worlds is embedded into the final SWF
 * file and located in ResourceManager.as. The currentWorld variable is responsible for tracking which world the user is currently
 * in, and EntranceObjects are used to manage entering and exiting worlds.
 * 
 * The outline of this file is:
 * 			1. Initialization of game variables (includes event dispatchers, initiating drawing resources for the world, etc...
 * 			2. Event handlers for events defined in GameProgressEvents as well as Application events (mouseclick, etc...)
 * 			3. EnterFrame section. Deals with the special event of what happens on a per-frame basis. Important to update this 
 * 				section whenever implementing a new function so it shows up, lol.
 * 
 * ******************************************************************************************************************************/
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.TextArea;
	import mx.controls.dataGridClasses.DataGridColumn;
	import mx.core.Application;
	import mx.events.ListEvent;
	
	/*default initialization of vars here*/
	private var gameObjectManager:GameObjectManager = null;
	private var messageDispatcher:MessageDispatcher = null;
	private var chatRoomManager:ChatRoomManager = null;
	private var OPManager:OtherPlayerManager = null;
	private var comet2Manager:Comet2ManagerImpl = null;
	private var OP:OtherPlayer = null;
	private var world:World = null;
	private var scrollX:Number = 0;
	private var scrollY:Number = 0;
	private var currentWorld:int = 1; //defines the current level the player is in (see ResourceManager.as)
	
	private var myUserName:String = null;
	public var player:Player = null;
	
	private var lastFrame:Date;
	private var lastWorldScrollX:Number=0;
	private var lastWorldScrollY:Number=0;
	private var lastPlayerPoint:Point = new Point(0,0); //records the last position of the player when he enters a new world
	private var frameTracker:int = 0;
	
	private var eventsDictionary:Dictionary = new Dictionary; //a dictionary ascribing event strings to the functions that handle them
	private var PriceData:Array = null; //collects information about prices and redistributes them.
	
	/*handles login checks*/
	private function CheckLogin():void
	{
		this.myUserName = userName.text;
		Alert.show("Konichiwa " + userName.text + ", you are logged in.");
		Application.application.removeChild(loginPanel);
		Application.application.currentState="Game";
		initGame(); //start your engines
	}
	
	/*Initialization of graphics and game resources*/
	private function initGame():void
	{
		lastFrame = new Date();
		this.gameObjectManager = new GameObjectManager();
		this.messageDispatcher = new MessageDispatcher(); //custom eventdispatcher for game events
		this.comet2Manager =  new Comet2ManagerImpl(); //custon eventDispatcher for COMET-specific events
		
		/*Event listeners need to be added to the application to be registered*/
		Application.application.addEventListener(MouseEvent.CLICK, click);
		Application.application.addEventListener(MouseEvent.MOUSE_DOWN, mouseDown);
		Application.application.addEventListener(MouseEvent.MOUSE_UP, mouseUp);
		Application.application.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
		
		/*Initializes important aspects of the game*/
		ResourceManager.InitialiseResources();
		ConversationManager.CreateConversations();
		GameProgressEventManager.CreateGameProgressEventManager(this.messageDispatcher);
		GameProgressEventManager.ObtainPlayerInfo(this.myUserName);
		
		/*identifies the initial world and sets the player properties*/
		this.world = ResourceManager.CreateWorld(this.messageDispatcher, this.gameObjectManager, currentWorld);
		this.player = new Player(this.messageDispatcher, this.gameObjectManager, new Point(80, 0), this.world, this.currentWorld);
		this.player.set_NameID = this.myUserName;
		
		/*initializes connection with network*/
		var levelWidth:int = this.world.getCols()*this.world.TileWidth
		var levelHeight:int = this.world.getRows()*this.world.TileHeight
		this.OPManager = new OtherPlayerManager(this.world, this.player, this.messageDispatcher, this.comet2Manager, this.gameObjectManager, levelWidth, levelHeight);
		OPManager.initConnection();
		
		AddEventListeners();
		
		/*allows chatroom to connect*/
		this.chatRoomManager = new ChatRoomManager(this.player.NameID, this.messageDispatcher, this.comet2Manager, this.player);
		
		this.addEventListener(Event.ENTER_FRAME, enterFrame);
	}	
	
	/*Adds event listeners to all the appropriate managers.*/
	private function AddEventListeners():void
	{
		PopulateEventsDictionary();
		AddCustomEventListeners();
		
		if(!this.comet2Manager.hasEventListener(ChatRoomEvent.CHAT_ROOM_LIST_USERS) && this.player.ChatActive)
		{
			this.comet2Manager.addEventListener(ChatRoomEvent.CHAT_ROOM_LIST_USERS, showChatUsers);
		}
		
		if(!this.comet2Manager.hasEventListener(ChatRoomEvent.CHAT_ROOM_EXIT))
		{
			this.comet2Manager.addEventListener(ChatRoomEvent.CHAT_ROOM_EXIT, ExitChatRoom);
		}
		
		
	}
	
	//populates events Dictionary with necessary events. Less-efficient, but neater approach than simply adding event listeners.
	private function PopulateEventsDictionary():void
	{
		//for some reason you have to add PLAYER_STOP_MOVE manually. I have no idea why; the others work well
		this.messageDispatcher.addEventListener(GameProgressEvent.PLAYER_STOP_MOVE, playerStopMove); 
		
		this.eventsDictionary = new Dictionary();
		//this.eventsDictionary[GameProgressEvent.PLAYER_STOP_MOVE] = playerStopMove as Function;
		this.eventsDictionary[GameProgressEvent.PLAYER_START_MOVE] = playerStartMove as Function;
		this.eventsDictionary[GameProgressEvent.CLICKED_COLLISION_TILE] = restoreMouseClickListener as Function;
		this.eventsDictionary[GameProgressEvent.DISPLAY_ACTIONS] = displayActions as Function;
		this.eventsDictionary[GameProgressEvent.IN_FRONT_OF_PORTAL] = displayActions as Function;
		this.eventsDictionary[GameProgressEvent.ENTER_WORLD] = jumpToNewWorld as Function;
		this.eventsDictionary[GameProgressEvent.EXIT_WORLD] = jumpToNewWorld as Function;
		this.eventsDictionary[GameProgressEvent.WITHIN_CHATTING_DISTANCE] = displayActions as Function;
		this.eventsDictionary[GameProgressEvent.LAUNCH_CHATBOX] = EnterChatRoom as Function;
		
		if(this.currentWorld==1) 
		{
			this.eventsDictionary[GameProgressEvent.TALK] = talk as Function;
			this.eventsDictionary[GameProgressEvent.END_TALK] = endTalk as Function;
		}
		
		if(this.currentWorld==2)
		{
			
		}
	}
	
	/*adds appropriate action listeners to their respective functions to */
	private function AddCustomEventListeners():void
	{
		for(var key:String in this.eventsDictionary) //SELF-NOTE: Iterates thru keys, not value
		{
			
			if(!this.messageDispatcher.hasEventListener(key)) 
			{
				this.messageDispatcher.addEventListener(key, this.eventsDictionary[key]);
			}
		}
	}
	
	
	/*=====================================================================================================
	* This section of code deals with functions that handle specified events such as when the player 
	* approaches an item that can be picked up, encounters a seller, etc. Event listeners are added 
	* in the appComplete function just above for these functions. These functions depend on 
	* the classes GameProgressEvents and InteractionGameObjects (and the classes that extend these 
	* classes) to listen to and handle various events that are fired throughout the game.
	* =======================================================================================================*/ 
	
	/*Here you can control specifically which jump to make to which world, depending
	on whether specific quests or actions have been completed.
	If the specific world is to be entered and can be returned to, then the player's last
	position must be memorized.
	Consequently, when we exit a world, we must call upon the last position remembered to
	return to.*/
	private function jumpToNewWorld(event:GameProgressEvent):void
	{
		
		if(event.type.toString()=="Action_EnterWorld")
		{   //if entering world, we remember the last location from which current player entered 
			lastWorldScrollX = scrollX;
			lastWorldScrollY = scrollY;
			lastPlayerPoint = this.player.Position;
		}
		
		var eventInfo:int = event.ActionData as int; //data contains an int of the new world
		/*takes care of clearing the old world map, objects (including) players.*/
		this.currentWorld = eventInfo;
		this.actionBox.removeAllChildren();
		gameObjectManager.shutdown();
		
		/*recreates objects, players, otherPlayers, etc*/
		this.messageDispatcher = new MessageDispatcher();
		this.world = ResourceManager.CreateWorld(this.messageDispatcher, this.gameObjectManager, this.currentWorld);
		this.OPManager.CurrentLevel = this.currentWorld;
		
		//repopulates event dictionary and calls appropriate addEvenListener functions
		AddEventListeners();
		
		var playerPoint:Point = new Point(0,0);
		if(event.type.toString()=="Action_ExitWorld")
		{   //when exiting, we recreate the position the player left in the old world
			scrollX = lastWorldScrollX;
			scrollY = lastWorldScrollY;
			playerPoint = lastPlayerPoint;
		}	
		
		this.player = new Player(this.messageDispatcher, this.gameObjectManager, playerPoint, this.world, this.currentWorld);
		this.player.set_NameID = this.myUserName;
		
		var levelWidth:int = this.world.getCols()*this.world.TileWidth
		var levelHeight:int = this.world.getRows()*this.world.TileHeight
		this.OPManager = new OtherPlayerManager(this.world, this.player, this.messageDispatcher, this.comet2Manager, this.gameObjectManager, levelWidth, levelHeight);
		this.OPManager.initConnection();
	}
	
	private function playerStartMove(event:GameProgressEvent):void
	{
		this.actionBox.removeAllChildren();
	}
	
	private function playerStopMove(event:GameProgressEvent):void
	{
		Application.application.addEventListener(MouseEvent.CLICK, click);
	}
	
	private function restoreMouseClickListener(evt:GameProgressEvent):void
	{
		Application.application.addEventListener(MouseEvent.CLICK, click);
	}
	
	/*if another user wants to talk to the current user, a request message is sent*/
	private function onChatRequest(event:ChatRoomEvent):void
	{
		var chatRequestButton:Button = new Button;
		chatRequestButton.label = event.information + " wants to chat";
		chatRequestButton.addEventListener(MouseEvent.CLICK, 
			function (event:MouseEvent):void
			{
				this.messageDispatcher.dispatchEvent(new ChatRoomEvent(ChatRoomEvent.CHAT_ROOM_ENTER, 
					this.player.NameID, ""));	
			}
		);
		this.actionBox.addChild(chatRequestButton);
	}
	
	
	/*called upon when the user decides to enter into a chat state with another user.*/
	private function EnterChatRoom(event:ChatRoomEvent):void
	{
		this.currentState = "ChatRoom";
	}
	
	private function ExitChatRoom(event:ChatRoomEvent):void
	{
		this.currentState = "Game";
	}

	private function showChatUsers(event:ChatRoomEvent):void
	{
		
	}
	
	/*function displayActions: Called on when a player nears an object capable of being picked up.
	* Displays each action that can be performed on the object using buttons.
	* ActionData is an array with each entry of the object form: { name: "", event: ""}
	*/
	private function displayActions(evt:GameProgressEvent):void
	{
		
		var actions:Array = evt.ActionData as Array;
		if (actions != null)
		{
			for each (var action:Object in actions)
			{
				var actionButton:Button = new Button();
				actionButton.label = action.name;
				actionButton.addEventListener(
					MouseEvent.CLICK, 
					function(event:MouseEvent):void
					{
						Application.application.actionBox.removeAllChildren();
						Application.application.messageDispatcher.dispatchEvent(
							new GameProgressEvent(action.event, GameObjectIDs.PLAYER, evt.SenderID));															
					}
				);
				this.actionBox.addChild(actionButton);
			}
		}
	}		
	
	/* This section deals with initialization and handling of conversation events by working with
	* the ConversationManager and ConversationText classes. The functions themselves are 
	* pretty self explanatory.
	*/
	private function endTalk(event:GameProgressEvent):void
	{
		this.conversationWindow.text = "";
		this.conversationResponseBox.removeAllChildren();
		this.currentState = "Game";				
	}
	
	private function talk(event:GameProgressEvent):void
	{				
		
		var conversationID:int = event.ActionData.conversationId;
		var name:String = event.ActionData.name;
		var sender:int = event.SenderID;
		var conversation:Conversation = ConversationManager.GetConversation(conversationID);
		if (conversation != null)
		{
			var conversationText:ConversationText = conversation.getConversationEntryPoint();
			
			if (conversationText != null)
			{					
				this.currentState = "Talk";
				this.talkPanel.title = name;
				this.displayConversationText(conversationText, sender);
			}
		}
	}
	
	private function displayConversationText(conversationText:ConversationText, sender:int):void
	{
		this.conversationResponseBox.removeAllChildren();
		this.conversationWindow.text += conversationText.Text + "\n\n";
		for each (var responseID:int in conversationText.ResponseIDs)
		{
			var conversationResponse:ConversationResponse = 
				conversationText.ParentConversation.getConversationResponse(responseID);
			
			if (conversationResponse != null)
				displayConversationResponse(conversationResponse, sender);
		}
	}
	
	private function displayConversationResponse(conversationResponse:ConversationResponse, sender:int):void
	{
		var application:main = this;
		
		
		var button:Button = new Button();
		button.label = conversationResponse.Text;
		button.addEventListener
			(
				MouseEvent.CLICK, 
				function (event:MouseEvent):void
				{
					if (conversationResponse.ConversationID == Conversation.END_CONVERSATION)
					{
						application.messageDispatcher.dispatchEvent(
							new GameProgressEvent(GameProgressEvent.END_TALK, sender));
					}
					else
					{
						application.conversationWindow.text += "You said: " + conversationResponse.Text + "\n\n";
						application.displayConversationText(
							conversationResponse.ParentConversation.getConversationText(conversationResponse.ConversationID), sender);
					}
				}
			);
		this.conversationResponseBox.addChild(button);
	}	
	
	/*Mouse event handlers. Action listeners added upon entering into the game state.
	* Each function calls on a gameObjectManager function, which redraws the map
	* according to how the user clicked.*/
	private function click(event:MouseEvent):void
	{
		
		if (event.target == this.canvas)
		{	/*MouseListener is removed from the application and only readded when the player has finished moving;
			makes moving consistent between players by slightly restricting movement*/
			this.removeEventListener(MouseEvent.CLICK, click);
			this.gameObjectManager.click(event, int(scrollX), int(scrollY));
		}
	}
	
	
	private function mouseDown(event:MouseEvent):void
	{
		if (event.target == this.canvas)
			this.gameObjectManager.mouseDown(event, int(scrollX), int(scrollY));
	}
	
	private function mouseUp(event:MouseEvent):void
	{
		if (event.target == this.canvas)
			this.gameObjectManager.mouseUp(event, int(scrollX), int(scrollY));
	}
	
	private function mouseMove(event:MouseEvent):void
	{
		if (event.target == this.canvas)
			this.gameObjectManager.mouseMove(event, int(scrollX), int(scrollY));
	}	
	
	
	/* key functions implemented but contains a weird geoemtric error; hence they are disabled
	private function keyDownHandle(event:KeyboardEvent):void
	{	//DISABLED FUNCTION
	if (event.target == this.canvas)
	this.gameObjectManager.keyDownHandle(event, int(scrollX), int(scrollY));
	}
	*/
	
	/*=============================================================================================== 
	* ENTERFRAME
	* An important fucntion that describes which objects need to be updated at every frame. This does
	* not include the process of going from one world to another; that is taken care of by jumpWorld.
	* Instead, this function redraws the graphics on the screen as needed, while updating objects to 
	* be drawn on the screen if needed. 
	* ===============================================================================================*/ 
	
	/*EnterFrame:
	Draws the map onto the canvas and limits the world scrolling.
	Add to this section any per-frame changes.
	Also introduces a timeStamp.*/
	private function enterFrame(event:Event):void
	{	
		// Calculate the time since the last frame
		var thisFrame:Date = new Date();
		var seconds:Number = (thisFrame.getTime() - lastFrame.getTime())/1000.0;
		lastFrame = thisFrame;
		
		var canvasGraphics:Graphics = this.canvas.graphics;	
		var surface:BitmapData = new BitmapData(Application.application.width, Application.application.height, true);
		
		// player starts every map at upper left hand corner of map
		this.scrollX=0;
		this.scrollY=0;
		
		//scroll only if the map size is greater than that of the window
		if(world.Tiles[0].length > this.width/world.TileWidth
			&& world.Tiles[0][0].length > this.height/world.TileHeight)
		{
			//centre on the player
			scrollX = this.player.Position.x - this.width / 2;
			scrollY = this.player.Position.y - this.height / 2;
			
			// limit the scrolling
			var maxXScroll:int = (world.TileWidth * world.Tiles[0].length) - this.width;
			var maxYScroll:int = (world.TileHeight * world.Tiles[0][0].length) - this.height;
			
			if (scrollX < 0) scrollX = 0;
			else if (scrollX > maxXScroll) scrollX = maxXScroll;
			
			if (scrollY < 0) scrollY = 0;
			else if (scrollY > maxYScroll) scrollY = maxYScroll;
			
			this.OPManager.ScrollX = scrollX; //must update for OPManager
			this.OPManager.ScrollY = scrollY;
		}
		
		// update the game objects
		this.gameObjectManager.enterFrame(seconds);
		
		// draw the ground layer
		world.drawWorldLayer(0, surface, int(scrollX), int(scrollY));
		
		// draw the player layer
		this.gameObjectManager.drawObjects(surface, int(scrollX), int(scrollY));
		
		
		// draw the remaining layers
		for (var layers:int = 1; layers < world.Tiles.length; ++layers)
			world.drawWorldLayer(layers, surface, int(scrollX), int(scrollY));							
		
		canvasGraphics.clear();
		canvasGraphics.beginBitmapFill(surface, null, false, false);
		canvasGraphics.drawRect(0, 0, this.canvas.width, this.canvas.height);
		canvasGraphics.endFill();
		
		this.frameTracker++;
	}	

