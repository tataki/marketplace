package
{
	public class WorldTile
	{
		protected var fileName:String = "";
		protected var width:int = 0;
		protected var height:int = 0;
		protected var startX:int = 0;
		protected var startY:int = 0;
		
		public function get FileName():String
		{
			return fileName;
		}
		
		public function get Width():int
		{
			return width;
		}
		
		public function get Height():int
		{
			return height;
		}
		
		public function get StartX():int
		{
			return startX;
		}
		
		public function get StartY():int
		{
			return startY;
		}
		
		public function WorldTile(fileName:String, width:int, height:int, startX:int, startY:int)
		{
			this.fileName = fileName;
			this.width = width;
			this.height = height;
			this.startX = startX;
			this.startY = startY;
		}

	}
}