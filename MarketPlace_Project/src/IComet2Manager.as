package
{
	import flash.events.IEventDispatcher;
	
	/**
	 * Flex port of the Javascript new Comet Library.
	 * 
	 * This Library allow to listen to specific events
	 * (corresponding to String identifiers).
	 * 
	 * An event have 3 parameters:
	 * - [dest] String identifying the event channel you want to listen for (your event).
	 * - [sender] String corresponding to the sender ID
	 * - [data] Object containing the data you want to send. Data must be JSONable
	 * - [target] Optional parameter: specify the destination of the message
	 * 
	 * When you want to listen for events,
	 * you must tell:
	 * - [listen] Array of Strings, containing the events channels you want to listen.
	 * - [ignore] Array of Strings, containing the sender IDs you want to ignore (generally -> yours)
	 * - [key] a unique key identifying you for the Comet Server.
	 * - [unicast] Array of Strings, the user IDs you want to use for unicast messages.
	 * 
	 * @see CometManagerEvent
	 * 
	 * @author Jonathan Martin
	 **/
	public interface IComet2Manager extends IEventDispatcher
	{
	
		/**
		 * Start Listening for events (listed in listen parameter),
		 * while ignoring events comming from user with IDs in the ignore list parameter.
		 * You must specify a UNIQUE key to identify this instance from the server side point of view.
		 * 
		 * Each time a correspondig event is received, a COMET_DATA_RECEIVED event will be triggered.
		 * 
		 * @param listen Array of Strings, containing the events you want to listen.
		 * @param ignore Array of Strings, containing the sender IDs you want to ignore (generally -> yours).
	 	 * @param key a unique key identifying you for the Comet Server.
	 	 * 
	 	 * @see CometManagerEvent.COMET_DATA_RECEIVED
		 **/
		function startReceiving(listen:Array, ignore:Array, unicast:Array=null):void;
		
		/**
		 * Stop receiving messages from Comet.
		 **/
		function stopReceiving():void;
		
		/**
		 * Send a message to the CometServer.
		 * When the message will be tranmitted, a COMET_REQUEST_SENT event will be triggered.
		 * If an error occurs, the the COMET_REQUEST_ERROR event is triggered.
		 * 
		 * @param dest String identifying the event you want to listen.
		 * @param sender String corresponding to the sender ID
		 * @param data Object containing the data you want to send. Data must be JSONable
		 * @param target String specifying the destination of the message
		 * 
		 * @see CometManagerEvent.COMET_REQUEST_SENT
		 * @see CometManagerEvent.COMET_REQUEST_ERROR
		 **/
		function sendData(dest:String, sender:String, data:Object, target:String=null):void;
		
	}
}