package{
	public interface LoggerIF
	{
		function logOutput(log:String,logLevel:int):void;
		function error(log:String):void;
		function info(log:String):void;
		function debug(log:String):void;
		function get outputLevel():int;
		function set outputLevel(level:int):void;
	}
}