//Actionscript
package
{
	import mx.controls.TextArea;

	public class TextPadLogger implements LoggerIF
	{		
		// GLOBAL SETTINGS
		private static var _textArea:TextArea = null;
		private static var _logText:String = "";
		private static var _config:TextPadLoggerConfig = new TextPadLoggerConfig();
		
		// LOCAL SETTINGS
		private var _outputLevel:int;
		
		public static function getInstance(loggerName:String=null):TextPadLogger {
			return new TextPadLogger(loggerName);
		}
		
		public function TextPadLogger(loggerName:String=null)
		{
			this._outputLevel = _config.getLevel(loggerName);
		}
		
		public static function setTextArea(textArea:TextArea):void {
			_textArea = textArea;
			if (_textArea != null) {
				_textArea.htmlText = _logText;
			}
		}

		public function logOutput(log:String, logLevel:int):void
		{
			var usedLevel:int = Math.max(this._outputLevel, _config.defaultLevel);
			
			//ログレベルが無い場合は何も出力しない。
			if (usedLevel == LogLevel.NONE) {
				return;
			}
			
			//ログレベルが合っていれば出力する。
			if(usedLevel >= logLevel) {
				var prefix:String;
				var suffix:String = "";
				switch (logLevel) {
					case LogLevel.NONE :
						prefix = "[NONE ]";
						break;
					case LogLevel.ERROR :
						prefix = "<font color='#AA0000'>[ERROR]";
						suffix = "</font>";
						break;
					case LogLevel.INFO :
						prefix = "[INFO ]";
						break;
					case LogLevel.DEBUG :
						prefix = "<font color='#006600'>[DEBUG]";
						suffix = "</font>";
						break;
					default :
						prefix = "";
						break;
				}
				
				trace(log);
				var outputLog:String = prefix + log + suffix + "\n";
				_logText += outputLog;
				if (_textArea != null) {
					_textArea.htmlText += outputLog;
					// auto scroll down
					if(_config.autoScroll){
						_textArea.validateNow();
						_textArea.verticalScrollPosition = _textArea.maxVerticalScrollPosition;
					}
				}
			}
		}

		public function error(log:String):void
		{
			this.logOutput(log, LogLevel.ERROR);
		}

		public function info(log:String):void
		{
			this.logOutput(log, LogLevel.INFO);
		}

		public function debug(log:String):void
		{
			this.logOutput(log, LogLevel.DEBUG);
		}
		
		public function get outputLevel():int
		{
			return this._outputLevel;
		}
		
		public function set outputLevel(level:int):void
		{
			this._outputLevel = level;
		}
		
		public static function isAutoScrolling():Boolean
		{
			return _config.autoScroll;
		}
		
		public static function setAutoScrolling(autoscroll:Boolean):void
		{
			_config.autoScroll = autoscroll;
		}
		
		public static function getDefaultOutputLevel():int
		{
			return _config.defaultLevel;
		}
		
		public static function setDefaultOutputLevel(lvl:int):void
		{
			_config.defaultLevel = lvl;
		}
	}
}
