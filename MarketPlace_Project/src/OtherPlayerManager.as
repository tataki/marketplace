package
{
	/**=======================================================================================================
	OTHERPLAYERSMANAGER CLASS
		This class takes care of managing arrays and sending-and-receiving of information about otherplayers
		objects, or objects that represent other players on the server. These objects represent relatively the
		the positions and some information about other players.
		
		 * Clients of this class must first call ont eh constructor, then the initalizeConnection method on 
		 * the object to start listening. 
		 
		An instance of this class is initialized in the beginning of the game and listens for DATA_RECEIVED events.
		When these events are received, they are processed and gameObjectManager.newBaseObjects is updated
		to reflect this change so that when we next enter a frame, new characters may be drawn.
		When we enter a new world, this class needs to request data about the new world, specifically the new
		height and widths.
	*=========================================================================================================*/
	
	public class OtherPlayerManager
	{
		import flash.geom.Point;
		import flash.utils.Dictionary;
		
		import mx.collections.ArrayCollection;
		import mx.controls.Alert;
		import mx.utils.Base64Decoder;
		
		//defines the range, in tile counts, of the "detectable" screen for the player, e.g. the zone in which other players would be detected and drawn.
		private static const BORDER_:int = 5; 
		private static const BASE_OTHER_PLAYER_ID:int = 200000;
		
		//Stores the ID's of all players currently drawn on the screen, for referencing players
		private var onScreenPlayers:ArrayCollection = new ArrayCollection();
		
		private var manager:IComet2Manager = null;
		protected var messageDispatcher:MessageDispatcher = null;
		protected var gameObjectManager:GameObjectManager = null;
		private var world:World = null;
		private var currentLevel:int = -1;
		private var Application_HEIGHT:int = -1; //in pixels
		private var Application_WIDTH:int = -1; //in pixels
		private var scrollX:int = 0; //in pixels; default is the upper left corner
		private var scrollY:int = 0;
		private var referencePlayer:Player = null;
		
		private var DEFINE_WIDTH_CLOSE_TO_PLAYER:int = -1;
		private var DEFINE_HEIGHT_CLOSE_TO_PLAYER:int = -1;
	
		
		//For receiving messages: these are all string-arrays.
		private var listenTo:Array = null; //identifies the channel names that will be listened to
		private var ignoredUsers:Array = null; //identifies userNames to be ignored; usually this is your own name
		private var myKey:String = null;
		private var uniCastMessage:Array = null;
		
		
		public function get CurrentLevel():int
		{
			return currentLevel;
		}
		
		public function set CurrentLevel(level:int):void
		{
			this.currentLevel = level;
			removeOtherPlayers();
		}
		
		public function get APP_HEIGHT():int
		{
			return this.Application_HEIGHT;
		}
		
		public function set APP_HEIGHT(height:int):void
		{
			this.Application_HEIGHT = height;
		}
		
		public function get APP_WIDTH():int
		{
			return this.Application_WIDTH;
		}
		
		public function set APP_WIDTH(width:int):void
		{
			this.Application_WIDTH = width;
		}
		
		public function set CurrentChannel(channel:String):void
		{
			this.listenTo = new Array();
			this.listenTo.push(channel);
		}
		
		public function set ScrollX(scrollX:int):void
		{
			this.scrollX = scrollX;
		}
		
		public function set ScrollY(scrollY:int):void
		{
			this.scrollY = scrollY; 
		}
		
		
		/**Initialization of an OtherPlayerManager. Only one is needed per application. Both this and the initializeConnection function need to be used
		 * whenever a new world is entered to refresh data having to do with the world and having to do with the application
		 * 
		 * @param world: Reference to the world class
		 * @mainPlayer: Information about the player accessing the application
		 * @param messageDispatcher: Access to the master object that collects and redistributes events
		 * @param gameObjectManager: Access to the gameObjectManager in the master application so that we modify its baseObjects class
		 * @param width: the width of the current map
		 * @param height: the height of the current map
		 */
		public function OtherPlayerManager(world:World, mainPlayer:Player, messageDispatcher:MessageDispatcher, cometManager:Comet2ManagerImpl, gameObjectManager:GameObjectManager, width:int, height:int)
		{
				this.Application_HEIGHT = height;
				this.Application_WIDTH = width;
				this.world = world;
				this.messageDispatcher = messageDispatcher;
				this.gameObjectManager = gameObjectManager;
				this.referencePlayer = mainPlayer;
				this.currentLevel = this.referencePlayer.CurrentWorld;
				this.manager = cometManager;
				
				//Define the detectable "zone" of otherplayers as BORDER_ number of tiles outside the viewable screen. Distance here measured in tiles, not pixels
				this.DEFINE_WIDTH_CLOSE_TO_PLAYER = (this.Application_WIDTH/(2*this.world.TileWidth) + BORDER_);
				this.DEFINE_HEIGHT_CLOSE_TO_PLAYER = (this.Application_HEIGHT/(2*this.world.TileHeight) + BORDER_);
				
		}
		
		/**Initialization of server connection. Listens for the appropriate channel, usually in the String format ACTION_WORLDNUM.
		 * The most basic channel to listen for is MOVE_1. These fields are updated every time a player enters a new world.
		 * 
		 * Utilizes variables internal to both the world and the main player object, which are assumed to be updated.
		 */
		public function initConnection():void
		{
			
			if(!manager.hasEventListener(Comet2ManagerEvent.COMET_DATA_RECEIVED)) manager.addEventListener(Comet2ManagerEvent.COMET_DATA_RECEIVED, messageReceived);

			this.listenTo = new Array();
			this.listenTo.push(this.referencePlayer.DestChannel + this.currentLevel);
			this.ignoredUsers = new Array();
			this.ignoredUsers.push(this.referencePlayer.NameID);
			this.uniCastMessage = null;
	
			startListen();
		}
		
		/**called upon when a new level is set. We have to go through, removing all otherPlayer objects that are in baseObjects and place
		 * them in removedBaseObjects. Then we remove all the players in onScreenPlayers.
		 */
		private function removeOtherPlayers():void
		{
			for each(var Name:String in onScreenPlayers)
			{
				for each(var gameObject:BaseObject in this.gameObjectManager.baseObjects)
				{
					if(gameObject.GameID >= BASE_OTHER_PLAYER_ID) this.gameObjectManager.removedBaseObjects.addItem(gameObject);
				}
			}
			
			this.onScreenPlayers = new ArrayCollection();
		}
		
		/*=======================================================================================================================
		SERVER INTERACTION
			Deals with interaction with the server. Opens up a listening channel to receive signals whenever they appear on the 
			network. Writes the sendMessage function to send a message containing useful data to the server to be broadcasted.
		========================================================================================================================*/
		
		/**Closes server connection; all channels are removed, and the data deleted from memory.
		 */
		public function stopListen():void 
		{
			this.manager.stopReceiving();
			Alert.show("Stopped listening");
		}
		
		private function startListen():void 
		{
			this.manager.startReceiving(this.listenTo, this.ignoredUsers, this.uniCastMessage);
			var output:String = "[";
			for(var j:int = 0; j < listenTo.length; j++) 
			{ 
				output += " " + listenTo[j] + ",";
			}
			output += "]"
		}
	
		
		/*===========================================================================================================
		HANDLERS SECTION
		These functions process events once they are close to completion.
		In particular, they decide the particular location at which a player is drawn
		=============================================================================================================*/
			
		/* FUNCTION MESSAGE RECEIVED
		 * called when a COMET message is received. The message contains an ID as well as a data point representing
		 * a player and his location.
		 * 
		 * UPDATING/ADDING: If the player is already on the screen, the player is simply moved to the new location.
		 * If the player was not there, the player is placed into the game's newBaseObjects array to be drawn at the next
		 * frame update.
		 * 
		 * REMOVAL: If the player goes out of range, it will no longer satisfy the boolean isNearPlayer and will be added to
		 * removedBaseObjects and removed in the next frame. The object's draw function will also be turned off.
		 * 
		 * DATA FORMAT: Data object is given in the following way: { Name: "Name" , WorldNumber: "1" , PointX: "40", PointY: "40", Guild: "otherGuild" }
		 * where PointX and PointY are the pixel representations of the updated position of the player
		*/
		private function messageReceived(event:Comet2ManagerEvent):void 
		{
			var msg:Comet2ReceivedMessage = event.content as Comet2ReceivedMessage;
			
			var otherData:Object = msg.data; //variable that stores information sent from the other player
		
			var newPosition:Point = new Point(parseInt(otherData.PointX), parseInt(otherData.PointY));
			var gameObject:BaseObject;
			if(this.onScreenPlayers.contains(otherData.Name)) 
			{//-------- here, we handle the case that the gameObject is already on the screen.
				//Alert.show("Player already on screen.");
				if(isNearPlayer(newPosition))
				{
					for each (gameObject in this.gameObjectManager.baseObjects)
					{
						if(gameObject.NameID == otherData.Name)
						{
							gameObject.Active = true;
							var newPt:Point = new Point(parseInt(otherData.PointX), parseInt(otherData.PointY));
							gameObject.move(newPt); //merely SETS the newPosition field of 
							break; //short-circuits as soon as it finds a player with that name to update
						}
					}
				} else { //player has moved out of range, and we find and delete that object.
					for each (gameObject in this.gameObjectManager.baseObjects)
					{
						if(gameObject.NameID == otherData.Name)
						{	//finds and stores the OtherPlayer object to be removed
							for each (gameObject in this.gameObjectManager.baseObjects)
							{
								if(gameObject.NameID == otherData.Name)
								{
									this.gameObjectManager.removeBaseObject(gameObject);
									break;
								}
							}
						}
					}
				}
			} 
			else //-------------------here we create a new player because the player is not yet drawn onto the screen as far as we know
			{ 
				if(isNearPlayer(newPosition))
				{   
					var newOP:OtherPlayer = createOtherPlayer(otherData);
					newOP.Active = true;
					newOP.move(new Point(parseInt(otherData.PointX), parseInt(otherData.PointY)));
					this.onScreenPlayers.addItem(otherData.Name);
				}
			}
		}
	
		//checks if the point in question is near the current player. Calculates closeness in terms of tiles, not pixels.
		private function isNearPlayer(newPosition:Point):Boolean
		{
			var playerTile:Point = this.world.pixelToTile(this.referencePlayer.Position);
			var localTile:Point = this.world.pixelToTile(newPosition);
			var xDist:int = Math.abs(playerTile.x - localTile.x);
			var yDist:int = Math.abs(playerTile.y - localTile.y);
			
			return (xDist <= DEFINE_WIDTH_CLOSE_TO_PLAYER) && (yDist <= DEFINE_HEIGHT_CLOSE_TO_PLAYER);
		}
		
		/*creates an OtherPlayer object; doesn't check for errors, so make sure that this point is accurate before calling
		rawData must have the following components:  { Name: "Name" , WorldNumber: "1" , PointX: "40", PointY: "40", Guild: "otherGuild" }
		*/
		private function createOtherPlayer(rawData:Object):OtherPlayer
		{
			var playerLevel:int = parseInt(rawData.WorldNumber);
			var playerGuild:String = rawData.Guild;
			var customGameID:int = (1 + BASE_OTHER_PLAYER_ID + this.onScreenPlayers.length);
			var playerPosition:Point = new Point(parseInt(rawData.PointX), parseInt(rawData.PointY));
			if(withinView(playerPosition)) { playerPosition = nearestValidSquare(this.scrollX, this.scrollY); }
			this.onScreenPlayers.addItem(rawData.Name);
			return new OtherPlayer(this.messageDispatcher, this.gameObjectManager, playerPosition,
				this.world, this.currentLevel, customGameID, rawData.Name);
		}
		
		/*returns the upper left hand corner if the player is not occupied; else returns the first position along the left panel that's not occupied.
			If its path is blocked, this would be an edge case that was not tested for, but in most cases, and in all maps created, the character will never
			be completely unable to access any part of the map.
		*/
		private function nearestValidSquare(pointX:int, pointY:int):Point
		{
			var result:Point = this.world.pixelToTile(new Point(pointX, pointY));
			while(this.world.isCollisionSquare(result)) 
			{
				result.x += 1;
			}
			return result;
		}

		/*Returns true if the specified point is within the world view of the current player*/
		private function withinView(pt:Point):Boolean
		{
			var playerTile:Point = this.world.pixelToTile(this.referencePlayer.Position);
			var otherPlayerTile:Point = this.world.pixelToTile(pt);
			var xDist:int = Math.abs(playerTile.x - otherPlayerTile.x);
			var yDist:int = Math.abs(playerTile.y - otherPlayerTile.y);
			
			if((xDist <= this.Application_WIDTH/(this.world.TileWidth)) && (yDist <= this.Application_HEIGHT/(this.world.TileHeight)))
			{
				Alert.show("withinView is true!");
				return true;
			}
			else
			{
				Alert.show("withinView is false!");
				return false;
			}
		}
		
	
	
	}
}