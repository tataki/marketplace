package
{
	import com.electrotank.astar.INode;
	import com.electrotank.astar.SearchResults;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	import flash.geom.Point;
	
	/**=======================================================================================================================
	   OTHERPLAYER CLASS
	 * This class is responsible for managing the representation for a character that is to be drawn in the vicinity of the player.
	 * These objects are managed by the OtherPlayerManager class, which stores a collection of these and connects to the network.
	 * 
	 * NOTES: 
	 * -An object of this class MUST be assigned a name. Other objects that extend the BaseObjects class have no nameID, but
	 * it is necessary for OtherPlayer objects to have name properties for referencing in gameObjectManagers.as.
	 * -When deciding to move an otherPlayer object, the baseObjects property "Active" MUST be set to true first.
	 * -When creating a new other 
	 * =======================================================================================================================*/
	 
	public class OtherPlayer extends GameObject
	{
		/*From players class; needed for movement*/ 	
		protected static const MOVE_TIME:Number = 0.4;
		protected var messageDispatcher:MessageDispatcher = null;
		protected var world:World = null;
		protected var path:Array = null;
		protected var nextTarget:int = -1;

		//variables exclusive to an OtherPlayer object
		private var myCurrentWorld:int = -1;
		private var nextPosition:Point = null;
		
		public function OtherPlayer(messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, position:Point, world:World, myCurrentWorld:int, gameID:int, name:String)
		{
			super(gameObjectManager, new GraphicsResource("player"), position, GameObjectIDs.OTHERPLAYER);
			this.world = world;
			this.myCurrentWorld = myCurrentWorld;
			this.messageDispatcher = messageDispatcher;
			this.gameID = gameID;
			this.nameID = name;
		}
		
		public function get CurrentWorld():int
		{
			return this.myCurrentWorld;	
		}
		
		public function set CurrentWorld(newWorld:int):void
		{
			this.myCurrentWorld = newWorld;
		}
		
		public override function enterFrame(dt:Number):void
		{
			if(this.IsActive == true) movePlayer(); 
		}
		
		public override function move(nextPoint:Point):void
		{
			this.nextPosition = nextPoint;
		}
		
		/*handles movement to a new data point; assumes that it was checked that the new player coordinates
		are close to that of the current player's coordinates.*/ 
		public function movePlayer():void
		{
			var newX:int = this.nextPosition.x;
			var newY:int = this.nextPosition.y;
			var oldX:int = this.position.x;
			var oldY:int = this.position.y;
			TweenLite.killTweensOf(this.position);
			//Alert.show("( " + scrollX.toString() + " , " + scrollY.toString() + " )");
			
			var newTile:Point = this.world.pixelToTile(new Point(newX, newY));			
			var currentTile:Point = this.world.pixelToTile(new Point(oldX, oldY));
			var result:SearchResults = world.search(currentTile.x, currentTile.y, newTile.x, newTile.y);

			if (result.getIsSuccess())
			{
				this.path = result.getPath().getNodes();
				this.nextTarget = 0;				
				moveToNextPoint();
				this.position = this.nextPosition;
				this.nextPosition = null;
				this.isActive = false; //player becomes inactive unless another signal is given to this player to move again
			}
		}
		
		protected function moveToNextPoint():void
		{
			++this.nextTarget;
			if (path != null && nextTarget < path.length)
			{
				var destinationNode:INode = path[nextTarget] as INode;
				var destinationXPoint:int = destinationNode.getCol() * this.world.TileWidth;
				var destinationYPoint:int = destinationNode.getRow() * this.world.TileHeight;
				
				/*tail-end recursive implementation of move function*/
				TweenLite.to(
					this.position, 
					MOVE_TIME, 
					{
						x:destinationXPoint, 
						y:destinationYPoint, 
						onComplete:moveToNextPoint, 
						ease:Linear.easeNone
					}
				);
			}
			else
			{
				path = null;
				nextTarget = 0;	
				messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PLAYER_STOP_MOVE, GameObjectIDs.PLAYER, this));
			}
		}
		
		
	}
}