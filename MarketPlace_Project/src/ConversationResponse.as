package
{
	public class ConversationResponse
	{
		protected var parentConversation:Conversation;
		protected var text:String = "";
		protected var conversationID:int = 0;
		
		public function get ParentConversation():Conversation
		{
			return parentConversation;		
		}
		
		public function set ParentConversation(parentConversation:Conversation):void
		{
			this.parentConversation = parentConversation;		
		}
		
		public function get Text():String
		{
			return text;		
		}
		
		public function get ConversationID():int
		{
			return conversationID;		
		}
		
		public function ConversationResponse(text:String, conversationID:int)
		{
			this.text = text;
			this.conversationID = conversationID;
		}

	}
}