package
{
	import flash.display.*;
	import flash.geom.*;
	
	/**=================================================================
	GAMEOBJECT class
	Adds a few extra properties to the BaseObjects; these objects
	generally have graphics and are defined by a certain position. 
	This class is extended by most kinds of objects, including the player
	avatar itself.
	since this class implements simply the drawing graphics and keeps
	track of the position of the object, in order to create new objects,
	new custom object classes should extend this one.
	===================================================================*/
	
	public class GameObject extends BaseObject
	{
		protected var position:Point = new Point(0, 0);
		protected var graphics:GraphicsResource = null;
		
		public function get Position():Point
		{
			return position;
		}
		
		public function get Graphics():GraphicsResource
		{
			return graphics;
		}
		
		public function GameObject(gameObjectManager:GameObjectManager, graphics:GraphicsResource, position:Point, gameID:int, zOrder:int = 0)
		{
			super(gameObjectManager, gameID, zOrder);	
			this.graphics = graphics;
			this.position = position.clone();
		}
		
		override public function shutdown():void
		{		
			super.shutdown();
			graphics = null;							
		}
		
		override public function draw(surface:BitmapData, scrollX:int, scrollY:int):void
		{
			var bitmap:BitmapData = ResourceManager.GetResource(graphics.ResourceName);
			var alphaBitmap:BitmapData = ResourceManager.GetAlphaResource(graphics.ResourceName);
			
			surface.copyPixels(
				bitmap, 
				bitmap.rect, 
				position.subtract(new Point(scrollX, scrollY)), 
				alphaBitmap, 
				new Point(0, 0), 
				true);
		}				
	}
}