package
{
	import com.serialization.json.*;
	
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	
	public class PaperochDatabaseManager
	{
		/**=======================================================================================================================
		 PaperochDatabaseManager class
		Interacts with the "marketplace" database in waitless to store and update information about a user's information.
		
		 
		 ========================================================================================================================*/
		
		//1 public static const ACCESS_URL_PREFIX:String = "/waitless"
		/* 2 */public static const WAITLESS_URL:String = "http://nexpiration.jp/waitless/100share/waitlesql/base/waitlesql-simple.scm.call";
		/*3*///public static const WAITLESS_URL:String = "http://nexpiration.jp/waitless/paperoch/server/paperoch-simple-sql.scm.call";
		
		public static const SELECT_ID_PRICE_COMMAND:String = "{\"sql\":\"SELECT name,value from marketplace.jimtest";
		public static const SELECT_PRICE_COMMAND:String = "{\"sql\":\"SELECT value from marketplace.jimtest";
		public static const INSERT_COMMAND:String = "{\"sql\":\"INSERT INTO marketplace.jimtest (id, type, value)";
		public static const UPDATE_COMMAND:String = "{\"sql\":\"UPDATE marketplace.jimtest set";
		//private static const SELECT_ALL_USERS_COMMAND:String = "{\"sql\":\"SELECT id from marketplace.jimtest";
		
		/*These are if you want to specify your own calls, or just write your own.*/
		public static const SELECT_START_COMMAND:String = "{\"sql\":\"SELECT ";
		public static const DATABASE_CALL_COMMAND:String = " from marketplace.jimtest ";
		public static const SQL_ENDING:String = "\"}";
		
		private var userID:String = null; //userID is the ID entered
		private var gameID:int = -1; //gameID identifies the MenuGameObject that called upon this menuItem to be created.
		private var itemNames:ArrayCollection = null;
		private var priceList:ArrayCollection = null;
		private var urlReq:URLRequest;
		private var urlLdr:URLLoader;
		private var messageDispatcher:MessageDispatcher = null;
		
		public function PaperochDatabaseManager(userID:String, messageDispatcher:MessageDispatcher, gameID:int) 
		{
			this.userID = userID;
			this.gameID = gameID;
			this.messageDispatcher = messageDispatcher;
			if(!this.messageDispatcher.hasEventListener(GameProgressEvent.LOOK_UP_DBASE_NAME)) {
				this.messageDispatcher.addEventListener(GameProgressEvent.LOOK_UP_DBASE_NAME, lookUpValueByName);
			}
		
		}
		
		/*Generic function that takes in a unique URLReqData, comprising the exact string that is to be requested, and a function that is to be
		 * called upon completion of the loading event.
		 * 
		 * URLRequestAddress: The URL address to make the request to
		 * URLReqData: The data (String or Binary data) that is to be sent as the body of the POST request
		 * CompletedFunction: The name of the function that is called upon receiving the data
		 * */
		private function UrlDataRequest(URLRequestAddress:String, URLReqData:String, CompletedFunction:Function):void
		{
			//TODO: IMPLEMENT THIS FUNCTION!!
			urlReq = new URLRequest(WAITLESS_URL);
			urlReq.contentType = "application/json";
			urlReq.method = URLRequestMethod.POST;
			urlReq.data = SELECT_ID_PRICE_COMMAND + " where type=" + userID + SQL_ENDING;
			
			/* Initialize the URLLoader object, assign the various event listeners, and load the specified URLRequest object. */
			urlLdr = new URLLoader();
			urlLdr.addEventListener(Event.COMPLETE, CompleteHandler);
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ErrorHandler);
			urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ErrorHandler);
			urlLdr.load(urlReq);
		}
		
		/**obtains the value (how much money) for the specified user. The data is returned in string form. 
		 * @param userID: The userID that one wants to obtain information for.
		 * */
		public function obtainIDInformation(userID:String):void
		{
			/* Initialize the URLRequest object with the URL to the file of name/value pairs. */
			urlReq = new URLRequest(WAITLESS_URL);
			urlReq.contentType = "application/json";
			urlReq.method = URLRequestMethod.POST;
			urlReq.data = SELECT_ID_PRICE_COMMAND + " where type=" + userID + SQL_ENDING;
			
			/* Initialize the URLLoader object, assign the various event listeners, and load the specified URLRequest object. */
			urlLdr = new URLLoader();
			urlLdr.addEventListener(Event.COMPLETE, CompleteHandler);
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ErrorHandler);
			urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ErrorHandler);
			urlLdr.load(urlReq);
		}
		
	
		
		public function StringUpdateRequest(ID:String, parameter:String, StrValue:String=null):void
		{
			urlReq = new URLRequest(WAITLESS_URL);
			urlReq.contentType = "application/json";
			urlReq.method = URLRequestMethod.POST;
			urlReq.data = UPDATE_COMMAND + " " + parameter + "=" + StrValue + " id=" + ID + " " + SQL_ENDING;
			
			/* Initialize the URLLoader object, assign the various event listeners, and load the specified URLRequest object. */
			urlLdr = new URLLoader();
			urlLdr.addEventListener(Event.COMPLETE, CompleteHandler);
			urlLdr.addEventListener(HTTPStatusEvent.HTTP_STATUS, ErrorHandler);
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ErrorHandler);
			urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ErrorHandler);
			urlLdr.load(urlReq);
		}
		
		/**sends a command to change things. The notation of this function is particular. Please follow convention:
			userID is unique and specifies when this object was added. userID's 1-999 and 10000+ are items. 1000-9999 are reserved for users. 
			type: 1 = user, 2 = object.
			price: must be an int.
			name: must be unique to the database (cannot have duplicate entries) and cannot be more than 20 chars.
			description: for type=1, the format is: {GUILD}_{POSITION}, e.g. DANCE_MASTER
						 for type=2, this should be a sentence or two describing the string, limited to no more than 1000 characters.
		*/
		public function insertInformation(userID:String, type:int, price:int, name:String, description:String):void
		{
			urlReq = new URLRequest(WAITLESS_URL);
			urlReq.contentType = "application/json";
			urlReq.method = URLRequestMethod.POST;
			urlReq.data = INSERT_COMMAND + " values=(" + userID + "," + type.toString() + "," + price.toString() + ")" + SQL_ENDING;
			
			/* Initialize the URLLoader object, assign the various event listeners, and load the specified URLRequest object. */
			urlLdr = new URLLoader();
			urlLdr.addEventListener(Event.COMPLETE, CompleteHandler);
			urlLdr.addEventListener(HTTPStatusEvent.HTTP_STATUS, ErrorHandler);
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ErrorHandler);
			urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ErrorHandler);
			urlLdr.load(urlReq);
		}
		
	
		
		/*Allows the data to display correctly. Needs to be formatted correctly to work with the database.*/
		private function categorizeData(rawData:Array):Array
		{
			var result:Array = new Array;
			for(var i:int =0; i < rawData.length; i+=2) 
			{
				var myObject:Object = {Name:"", Price:""};
				myObject.Name = rawData[i].toString();
				myObject.Price = rawData[i+1].toString();
				result.push(myObject);
			}
			return result;
		}
		
		
		/*=================================================================================================
		Event Handler Section. The following are event handlers that take care of certain events exchanged
		with other classes.
		==================================================================================================*/
		
		/*Here we complete the event by storing the resulting data in a GameProgressEvent object and having the main application receive it*/
		private function CompleteHandler(evt:Event):void 
		{		
			if(evt.type == Event.COMPLETE) {
				var loader:URLLoader = URLLoader(evt.target);
				var objectData:Object = JSON.decode(loader.data);
				
				if(objectData.toString().length != 0)
				{
					try { var rawPriceData:Array = objectData.toString().split(","); }
					catch(error:Error) { Alert.show(error.toString()); }
					var ProcessedArray:Array = categorizeData(rawPriceData);
					//this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.USER_IDENTIFIED, this.gameID, ProcessedArray));
				}
					
				else //the data is some kind of confirmation that we have completed an action.
				{
					Alert.show("Successful!");
				}
			}
		}
		
		/* The GameProgressEvent must have as its ActionData field a string representing the name of a user.
		It's a useful function for looking up individual entries' prices in the database. 
		The itemName must exactly match by character and case*/
		private function lookUpValueByName(evt:GameProgressEvent):void
		{
			var itemName:String = evt.ActionData.toString();
			urlReq = new URLRequest(WAITLESS_URL);
			urlReq.contentType = "application/json";
			urlReq.method = URLRequestMethod.POST;
			urlReq.data = SELECT_PRICE_COMMAND + " where name='" + itemName + "'" + SQL_ENDING;
			urlLdr = new URLLoader();
			urlLdr.addEventListener(Event.COMPLETE, returnName);
			urlLdr.addEventListener(IOErrorEvent.IO_ERROR, ErrorHandler);
			urlLdr.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ErrorHandler);
			urlLdr.load(urlReq);	
		}
		
		//helper function for lookUpValueByName. Dispatches an event and sends the actionData that represents the value of the requested name.
		private function returnName(evt:Event):void
		{
			var loader:URLLoader = URLLoader(evt.target);
			var objectData:Object = JSON.decode(loader.data);
			this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.LOOKED_UP_DBASE_NAME, -1, objectData.toString()));
			
			//Alert.show("Ok, dispatched");
		}
		
		public function successfulNotice(evt:Event):void
		{
			Alert.show("Purchase Successful!");
			
		}
		
		private function ErrorHandler(errorEvent:Event):void
		{
			Alert.show("Wahh: " + errorEvent.toString());
			//Alert.show("Uh oh, something went wrong; check what you typed in!");
		}
		
	}
}