package
{

	/*==========================================================================================
	Class PickUpGameObject
	* This class takes care of listening for certain events dispatched by other classes. The basic commands that
	* are triggered by the player moving toward a pick-up object are listened to in the constructor. 
	* These events trigger multiple events to occur in the game, forming a tree of commands. 
	===========================================================================================*/

	import flash.display.BitmapData;
	import flash.geom.Point;
	import mx.controls.Alert;
	
	public class PickupGameObject extends InteractionGameObject
	{
		protected var pickupProgressMarkerID:int = -0;
		
		protected function get PickupActionString():String
		{
			return "Pickup " + itemName;
		}
		
		public function PickupGameObject(world:World, messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, itemName:String, graphics:GraphicsResource, position:Point, gameID:int, pickupProgressMarkerID:int, zOrder:int = 0)
		{
			super(world, messageDispatcher, gameObjectManager, itemName, graphics, position, gameID, zOrder);		
			this.messageDispatcher.addEventListener(GameProgressEvent.PLAYER_STOP_MOVE, playerStopMove);
	
			this.pickupProgressMarkerID = pickupProgressMarkerID;	
		}
		
		protected function pickup(event:GameProgressEvent):void
		{
			if (event.ActionData == this.gameID)
			{
				this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PICKUP_OBJECT, this.gameID));
			}
		}

		/*playerStopMove
		* If the player is within a certain distance, has stopped moving, and the object has not been picked up,
		* a DISPLAY_ACTION event is dispatched. This event is then heard by the a button in the main mxml application.
		* The actionData field (last line) passes whatever is deemed useful information to the main application.*/
		protected function playerStopMove(event:GameProgressEvent):void
		{
			if (!GameProgressEventManager.GameProgressMarkerCompleted(this.pickupProgressMarkerID))
			{
				var player:Player = event.ActionData as Player;
				if (player != null && this.playerWithinActionDistance(player.Position))
				{
					this.messageDispatcher.dispatchEvent(
						new GameProgressEvent(
							GameProgressEvent.DISPLAY_ACTIONS,
							this.gameID, 
							[{name: this.PickupActionString, event: GameProgressEvent.OBJECT_PICKED_UP}]));
				}
			}
		}
		
		/*override draw: Only if the object has not been picked up or bought do we keep drawing the object
		* This means that the pickUpProgressMarkerID must be false.*/
		public override function draw(surface:BitmapData, scrollX:int, scrollY:int):void
		{
			if (!GameProgressEventManager.GameProgressMarkerCompleted(this.pickupProgressMarkerID)) {
				super.draw(surface, scrollX, scrollY);
			}
		}
	}
}
