package
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	
	import mx.controls.Alert;
	
	public class AdvertiserGameObject extends InteractionGameObject
	{
		public function AdvertiserGameObject(world:World, messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, itemName:String, graphics:GraphicsResource, position:Point, gameID:int, zOrder:int = 0)
		{
			super(world, messageDispatcher, gameObjectManager, itemName, graphics, position, gameID, zOrder);		
			this.messageDispatcher.addEventListener(GameProgressEvent.PLAYER_STOP_MOVE, playerStopMove);
			
		}
		
		protected function playerStopMove(evt:GameProgressEvent):void
		{
			
		}
	
	}
}