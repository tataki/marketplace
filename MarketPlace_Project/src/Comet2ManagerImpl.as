package
{
	
	import com.serialization.json.JSON;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	
	import mx.controls.Alert;

	/**================================================================================================================
	COMET2MANAGERIMPL
		Implements IComet2Manager. Specifies the URLs to be sent and uses the approach of a URLRequest to 
		a scheme file on the Waitless Server. The specific filename is specified and can be edited to include 
		more functions. 
		As of right now, when receiving, the scheme file parses incoming data, and then opens up a listening channel 
		using mashup_rcv.scm. Or, when sending, calls on mashup_send.scm to send data (see Comet.send).
		Utilizes event handlers and triggers to handle the work done. 
	================================================================================================================*/
		

	public class Comet2ManagerImpl extends EventDispatcher implements IComet2Manager
	{
		public static var logger:TextPadLogger = TextPadLogger.getInstance("Common.Comet");
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//TODO: Uncomment out the speific URL constants that are being used and comment out others				//
		//======================================================================================================//
		// In particular, they must be appropriate scheme calls that make appropriate changes to the database.  //
		// IF necessary, create scheme files online to be accessed.												//
		// #3 is appropriate when uploading to the server.   													//
		// When testing locally, make use of #4.																//
		// If both fail, try #1 and #2. I haven't tested these.													//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//1 private static const ACCESS_URL_PREFIX:String = "http://192.168.1.21:8088/waitless";
		//2 private static const ACCESS_URL_PREFIX:String = "http://w1.nexpiration.jp:1088/waitless";
		//3 private static const ACCESS_URL_PREFIX:String = "/waitless"
		
		/* 4 */ private static const ACCESS_URL_PREFIX:String = "http://nexpiration.jp/waitless";
		
		private var ACCESS_URL_COMET_SEND_CMD:String = ACCESS_URL_PREFIX + "/100share/comet/"+version+"/cometsend.scm.call";
		private var ACCESS_URL_COMET_RECV_CMD:String = ACCESS_URL_PREFIX + "/100share/comet/"+version+"/cometrecv.scm.call";
	
		private static const version:String = "/base";
		//private static const version:String = "jonathan-test";
		
		// SERVICES
		private var receivingService:URLLoader;
		private var sendingService:URLLoader;
		
		// MANAGING RECEPTION
		private var cometRequest:URLRequest;
		private var listen:Array;
		private var ignore:Array;
		private var unicast:Array;
		
		// Local key
		private var key:String;
		
		private var sending:Boolean = false;
		private var toSend:Array = [];
		
		public function Comet2ManagerImpl(cometSendCommand:String=null, cometReceiveCommand:String=null)
		{
			if(cometReceiveCommand != null){
				ACCESS_URL_COMET_RECV_CMD = cometReceiveCommand;
			}
			
			if(cometSendCommand != null){
				ACCESS_URL_COMET_SEND_CMD = cometSendCommand;
			}
			
			// RECEIVING MESSAGES
			receivingService = new URLLoader();
			receivingService.addEventListener(Event.COMPLETE, messageReceived);
			receivingService.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			receivingService.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
			
			// SENDING MESSAGES
			sendingService = new URLLoader();
			sendingService.addEventListener(Event.COMPLETE, messageSent);
			sendingService.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
			sendingService.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorHandler);
		}
		
		private function errorHandler(event:Event):void {
			logger.error("Error: "+event);
		}
		
		public function startReceiving(listen:Array, ignore:Array, unicast:Array=null):void
		{
			cometRequest = new URLRequest(ACCESS_URL_COMET_RECV_CMD);
			cometRequest.method = URLRequestMethod.POST;
			cometRequest.contentType = "application/json";
			// Create a new key for receiving
			generateKey();
			Alert.show(key.toString());
			this.listen = listen;
			this.ignore = ignore;
			this.unicast = unicast;
			
			cometRequest.data = generateRecvRequest();
			getNextMessage();
		}
		
		public function sendData(dest:String, sender:String, data:Object, target:String=null):void
		{
			var req:String = generateSendRequest(dest, sender, data, target);
			if(sending){
				//enqueue data
				toSend.push(req);
			} else {
				sending = true;
				logger.debug("SEND MESG REQ: "+req);
				var u:URLRequest = new URLRequest(ACCESS_URL_COMET_SEND_CMD);
				u.method = URLRequestMethod.POST;
				u.contentType = "application/json";
				u.data = req;
				sendingService.load(u);
			}
		}
		
		public function stopReceiving():void
		{
			receivingService.close();
			
			//sendingService.close();
		}
		
		// PRIVATE METHODS
		private function generateKey():void {
			this.key = "FlexCometManager_"+new Date().getTime();
		}
		
		private function getNextMessage():void {
			logger.debug("SEND RECV REQ: "+cometRequest.data);
			receivingService.load(cometRequest);
			logger.debug("WAITING FOR MESSAGE...");
		}
		
		private function messageSent(event:Event):void {
			var recv:String = sendingService.data as String;
			logger.debug("MESSAGE SENT. RESP="+ recv);
			var cometevent:Comet2ManagerEvent;
			
			if( recv == "true") {
				// OK
				logger.debug("SENDING OK EVENT.");
				cometevent = new Comet2ManagerEvent(Comet2ManagerEvent.COMET_REQUEST_SENT);
			} else {
				logger.debug("SENDING ERROR EVENT.");
				cometevent = new Comet2ManagerEvent(Comet2ManagerEvent.COMET_REQUEST_ERROR, "true expected, found: "+recv);
			}
			
			dispatchEvent(cometevent);
			
			// see if other requests to send
			if(toSend.length > 0){
				var req:String = toSend.pop() as String;
				logger.debug("SEND MESG REQ: "+req);
				var u:URLRequest = new URLRequest(ACCESS_URL_COMET_SEND_CMD);
				u.method = URLRequestMethod.POST;
				u.contentType = "application/json";
				u.data = req;
				sendingService.load(u);
			} else {
				sending = false;
			}
		}
		
		private function messageReceived(event:Event):void {
			var recv:String = receivingService.data as String;
			logger.debug("MESSAGE RECEIVED. RESP="+ recv);
			
			var cometevent:Comet2ManagerEvent;
			
			if( recv == "timeout") {
				// not a problem
				logger.debug("Timeout reached, relaunching");
				getNextMessage();
			} else if( recv == "closed") {
				// not a problem
				logger.debug("Connection closed by server. Generating a new key and relaunching");
				generateKey();
				cometRequest.data = generateRecvRequest();
			} else {
				var response:Object = JSON.decode(recv);
				logger.debug("SENDING DATA RECEIVED EVENT.");
				var msg:Comet2ReceivedMessage = new Comet2ReceivedMessage();
				msg.channel = response.dest;
				msg.sender = response.id;
				msg.data = response.objdata;
				msg.target = response.target;
				cometevent = new Comet2ManagerEvent(Comet2ManagerEvent.COMET_DATA_RECEIVED, msg);
				dispatchEvent(cometevent);
				getNextMessage();
			}
			
		}
		
		private function generateRecvRequest():String {
			var res:String = "{\"listen\":"+jsonArray(this.listen)+
				", \"ignore\":"+jsonArray(this.ignore)+", \"key\":\""+this.key+"\"";
			if(this.unicast != null){
				res += ", \"unicast\":"+jsonArray(this.unicast)+"}";
			} else {
				res += "}";
			}
			return res;
		}
		
		private function generateSendRequest(dest:String, id:String, data:Object, target:String=null):String {
			var res:String = "{\"dest\":"+JSON.encode(dest)+", \"id\":"+JSON.encode(id)+
				", \"objdata\":"+JSON.encode(data);
			if(target != null){
				res += ", \"target\":"+JSON.encode(target)+"}";
			} else {
				res += "}";
			}
			return res;
		}
		
		private function jsonArray(a:Array):String {
			if(a == null){
				return "\"\"";
			}
			var res:String = "[";
			var len:int = a.length;
			var lenMinusOne:int = len - 1;
			for(var i:int = 0; i < len; i++){
				if(i < lenMinusOne){
					res += "\""+a[i]+"\", ";
				} else {
					res += "\""+a[i]+"\"";
				}
			}
			res += "]";
			return res;
		}
	}
}