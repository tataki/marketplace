package
{
	import com.electrotank.astar.INode;
	import com.electrotank.astar.SearchResults;
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	import flash.geom.Point;
	
	import mx.controls.Alert;
	
	
	/*===============================================================================================
	* Class that defines movement for random walking characters.
	* 
	* Please use this class only when
	* NOTE: This class contains some code for keyboard functions that were not implemented
	* due to a graphics offset. It's pretty easy to correct, but I abandoned it due to reasons cited
	* (see below for details). I think that would be an interesting alternative to mouses
	* as it allows the player to "explore" more of the world on their own without having 
	* the A-star algorithm simply tell them how to get there.
	* ==============================================================================================*/
	
	public class RandomWalker extends GameObject
	{
		protected static const MOVE_TIME:Number = 0.4;
		protected var messageDispatcher:MessageDispatcher = null;
		protected var world:World = null;
		protected var path:Array = null;
		protected var nextTarget:int = -1;
		private var lastX:int = 0;
		private var lastY:int = 0;
		private var TILE_WIDTH:int = 40;
		private var TILE_HEIGHT:int = 40;
		
		public function RandomWalker(messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager, position:Point, world:World)
		{
			super(gameObjectManager, new GraphicsResource("girl1NPC"), position, GameObjectIDs.PLAYER);
			this.world = world;
			this.messageDispatcher = messageDispatcher;	
			
		}
		
		private function get playerPosition():Point
		{
			return this.position;
		}
		
		/*responsible for random movement*/
		public override function enterFrame(dt:Number):void
		{
			var currentTime:Number = dt * 1000;
			//Alert.show(currentTime.toString());
			if(Math.round(currentTime % 10) == 0) {
				var determinant:int = (Math.random()*10000) % 4;
				if(determinant == 0) { moveInDirection(40, 40); }
				if(determinant == 1) { moveInDirection(-40, -40); }
				if(determinant == 2) { moveInDirection(40, -40); }
				if(determinant == 3) { moveInDirection(-40, 40); }
				
			}
		}
		
		/*overrides the click function from GameObjectManager. The hard drawing stuff gets taken care of by 
		other functions.*/ 
		public function moveInDirection(deltaX:int, deltaY:int):void
		{
			TweenLite.killTweensOf(this.position);
			//Alert.show("( " + scrollX.toString() + " , " + scrollY.toString() + " )");
			
			var clickTile:Point = this.world.pixelToTile(new Point(deltaX + this.position.x, deltaY + this.position.y));			
			var currentTile:Point = this.world.pixelToTile(this.position);
			var result:SearchResults = world.search(currentTile.x, currentTile.y, clickTile.x, clickTile.y);
			
			
			if (result.getIsSuccess())
			{
				this.path = result.getPath().getNodes();
				this.nextTarget = 0;				
				moveToNextPoint();
				
				//this.position = this.world.tileToPixel(clickTile);
				//messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PLAYER_START_MOVE, GameObjectIDs.NPC, this));
				//Alert.show("success");
			}
		}
		/******************************* Code for Key Controls *************************************************
		 * There was some weird rounding error that I couldn't overcome, so I decided against implementing it this way.
		 * Basically what happens is that when you press multiple events, integers are added in such a way such that
		 * this approach can have you moving between two tiles. It's a fixable error, but whatever.
		 * Besides, the focus is not quest-centered but ease of navigation. If this were a quest-based game then
		 * this section would be appropriate. Otherwise, it's better to implement this using mouse controls and 
		 * least-cost paths. Blah.
		 
		 public override function keyDownHandle(event:KeyboardEvent, scrollX:int, scrollY:int):void
		 {
		 TweenLite.killTweensOf(this.position);
		 
		 
		 var currentTile:Point = this.world.pixelToTile(this.position);
		 //var newTile:Point = this.world.pixelToTile(new Point(currentTile.x-1/(this.world.TileWidth),
		 //						           currentTile.y-1/(this.world.TileHeight));
		 
		 
		 
		 
		 
		 if(event.keyCode == 37) {			
		 if(currentTile.x - 1/(this.world >=0) {
		 arriveXPoint = this.position.x - this.world.TileWidth;
		 arriveYPoint = this.position.y;
		 }
		 }
		 
		 if(event.keyCode == 38) {				
		 if(this.position.y - this.world.TileHeight >=0) {
		 arriveXPoint = this.position.x;
		 arriveYPoint = this.position.y - this.world.TileHeight;
		 }
		 }
		 
		 if(event.keyCode == 39) {			
		 if(this.position.x + this.world.TileWidth <= this.world.GroundLayerSizeX) {
		 arriveXPoint = this.position.x + this.world.TileWidth;
		 arriveYPoint = this.position.y;
		 }
		 }
		 
		 if(event.keyCode == 40) {			
		 if(this.position.x + this.world.TileHeight <= this.world.GroundLayerSizeY) {
		 arriveXPoint = this.position.x;
		 arriveYPoint = this.position.y + this.world.TileHeight;
		 }
		 }
		 
		 TweenLite.to(
		 this.position, 
		 MOVE_TIME, 
		 {
		 x:arriveXPoint, 
		 y:arriveYPoint, 
		 ease:Linear.easeNone
		 }
		 );
		 
		 messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PLAYER_START_MOVE, GameObjectIDs.PLAYER, this));
		 
		 }
		 *********************************************************************************************************/	
		protected function moveToNextPoint():void
		{
			++this.nextTarget;
			if (path != null && nextTarget < path.length)
			{
				var destinationNode:INode = path[nextTarget] as INode;
				var destinationXPoint:int = destinationNode.getCol() * this.world.TileWidth;
				var destinationYPoint:int = destinationNode.getRow() * this.world.TileHeight;
				
				/*tail-end recursive implementation of move function*/
				TweenLite.to(
					this.position, 
					MOVE_TIME, 
					{
						x:destinationXPoint, 
						y:destinationYPoint, 
						onComplete:moveToNextPoint, 
						ease:Linear.easeNone
					}
				);
			}
			else //we reset for the next move call here
			{
				path = null;
				nextTarget = 0;	
				//messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.PLAYER_STOP_MOVE, GameObjectIDs.PLAYER, this));
			}
		}		
		
	}
}
