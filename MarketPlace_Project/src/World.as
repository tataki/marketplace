package
{
	import com.electrotank.astar.Astar;
	import com.electrotank.astar.INode;
	import com.electrotank.astar.ISearchable;
	import com.electrotank.astar.Node;
	import com.electrotank.astar.SearchResults;
	
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**================================================================================
	CLASS WORLD
	This class defines one "world" or, in gaming terms, one level.
	Does three major things:
	1. Relies on ResourceManager.as to supply the raw data in array form. 
	2. Draws the resources onto the screen and has a useful function pixelToTile 
	that is used to define behaviors for certain objects.
	3. Implements the ISearchable interface. The astar object is used to calculate
	the best path between two points, and thus must be a resource of any world. 
	================================================================================*/
	

	public class World implements ISearchable
	{
		protected var tiles:Array = null;
		protected var collisionMap:Array = null;
		protected var astar:Astar = null;
		protected var tileWidth:int = 0;
		protected var tileHeight:int = 0;
		
		public function get Tiles():Array
		{
			return tiles;
		}
		
		public function get TileWidth():int
		{
			return tileWidth;
		}
		
		public function get TileHeight():int
		{
			return tileHeight;
		}
		
		public function World(tileWidth:int, tileHeight:int, tiles:Array, collisionMap:Array)
		{
			this.tiles = tiles;
			this.collisionMap = collisionMap;
			this.tileHeight = tileHeight;
			this.tileWidth = tileWidth;
			this.astar = new Astar(this);
			this.astar.setAllowDiag(false);
		}	
		
		public function pixelToTile(pixel:Point):Point
		{
			return new Point(
				int(pixel.x / this.TileWidth),
				int(pixel.y / this.TileHeight));
		}
		
		public function tileToPixel(tilePt:Point):Point
		{
			return new Point(
				int(tilePt.x * this.TileWidth),
				int(tilePt.y * this.TileHeight));
		}
		
		public function search(startX:int, startY:int, finishX:int, finishY:int):SearchResults
		{
			return astar.search(this.getNode(startX, startY), this.getNode(finishX, finishY));			
		}
		
		public function getCols():int
		{
			if (collisionMap != null)
			{
				return collisionMap.length;
			}
			
			return 0;
		}
		
		public function isCollisionSquare(point:Point):Boolean
		{
			if(collisionMap[point.x][point.y] == 1) return true;
			else return false;
		}
		
		public function getRows():int
		{
			if (collisionMap != null)
			{
				return collisionMap[0].length;
			}
			
			return 0;
		}
		
		public function getNode(col:int, row:int):INode		
		{
			if (collisionMap != null)
			{
				var collisionValue:int = collisionMap[col][row];
				var node:Node = new Node(row, col, collisionValue == 0);
				node.setNodeId(row.toString() + col.toString());
				return node;
			}
			
			return null;
		}
		
		public function getNodeTransitionCost(n1:INode, n2:INode):Number
		{
			return 1;
		}	
		
		public function get GroundLayerSizeX():int 
		{
			return (tileWidth)*(this.tiles[0].length-1);
		}
		
		public function get GroundLayerSizeY():int 
		{
			return (tileHeight)*(this.tiles[0].length-1);
		}
		
		public function drawWorldLayer(layer:int, surface:BitmapData, scrollX:int, scrollY:int):void
		{
			// draw the ground layer
			if (this.tiles.length >= layer + 1)
			{										
				for (var x:int = 0; x < this.tiles[layer].length; ++x)
				{
					for (var y:int = 0; y < this.tiles[layer][x].length; ++y)
					{
						var tile:WorldTile = this.tiles[layer][x][y];
						
						if (tile != null)
						{
							var bitmap:BitmapData = ResourceManager.GetResource(tile.FileName);
							var alpha:BitmapData = ResourceManager.GetAlphaResource(tile.FileName);
							
							if (bitmap != null)
							{
								surface.copyPixels(
									bitmap, 
									new Rectangle(tile.StartX, tile.StartY, tile.Width, tile.Height),
									new Point((x * this.tileWidth) - scrollX, (y * this.tileHeight) - scrollY),
									alpha, 
					 				new Point(tile.StartX, tile.StartY), 
					 				true);
							}
						}
					}
				}
			}
		}
	}
}
