/*asks the user to specify whom he would like to buy from*/
			private function InputSeller(event:GameProgressEvent):void
			{
				var inputText:TextArea = new TextArea();	
				var button:Button = new Button();
				button.label = "Buy from Guild";
				button.addEventListener(MouseEvent.CLICK, 
					function (event:MouseEvent):void 
					{
						
						Application.application.actionBox.removeAllChildren();
						Application.application.messageDispatcher.dispatchEvent(
									new GameProgressEvent(
									GameProgressEvent.VIEW_PRICES, GameObjectIDs.PLAYER, inputText.text));
													
					}
				);
				this.actionBox.addChild(inputText);
				this.actionBox.addChild(button);
			}
		

			/* void displayPrice: Handles the event of a user approaching a sales object in the game. Allows the player
			* to view the prices of another user by displaying them in a datagrid. To purchase an item, 
			* the player refers to the ComboBox (a drop down list) and selects the name of appropriate program. 
			* In all actionHandler functions (which are written as anonymous functions here for this reason), data
			* access comes from using the PriceData array.
			* It's long for a fairly straightforward function.*/
			private function displayPrice(event:GameProgressEvent):void 
			{
				
				PriceData = event.ActionData as Array;	
				var button:Button = new Button();
				button.label = "View My Programs!";
				button.addEventListener(MouseEvent.CLICK, displayPurchaseData);  
				this.actionBox.addChild(button);
			}
			
			private function displayPurchaseData(event:MouseEvent):void
			{/* Must define the event handler within as addEventListener cannot handle extra parameters*/
				Application.application.actionBox.removeAllChildren();
				/*Setup for columns and dataGrid. Note that this is oddly the only way of setting the column names for the
				DataGrid. No other options exist that are cheaper other than a for-loop through a string array of properties*/
				var columns:Array = [];
				var myColumn:DataGridColumn;
				myColumn = new DataGridColumn( "Name" );
				myColumn.headerText = "Name";
				columns.push( myColumn );
				
				//myColumn = new DataGridColumn( "Description");
				//myColumn.headerText = "Description";
				//myColumn.wordWrap = true;
				//columns.push( myColumn);
				
				myColumn = new DataGridColumn( "Price" );
				myColumn.headerText = "Price";
				columns.push( myColumn );
				
				var displayGrid:DataGrid = new DataGrid();
				displayGrid.width = 400;
				displayGrid.columns= columns;
				displayGrid.dataProvider= PriceData;
				
				/*Setup for drop-down list that allows for purchases to be made*/
				var Names:Array = [];
				for(var i:int = 0; i < PriceData.length; i++) { Names.push(PriceData[i].Name); }
				var myComboBox:ComboBox = new ComboBox();
				myComboBox.dataProvider = Names;
				
				Application.application.actionBox.addChild(displayGrid);
				Application.application.actionBox.addChild(myComboBox);
				myComboBox.addEventListener(ListEvent.CHANGE, purchaseMade);
			}

			//makes a purchase and passes on the object to MenuGameObject.
			//Sorry this function is very messy and relies on interaction with a function in the class PDManager. For simplicity in writing I decided to use events and listeners, 
			//but the logic here is certainly a bit convoluted.
			private function purchaseMade(event:ListEvent):void 
			{	
				var objectName:String = event.currentTarget.selectedItem.toString();
				var objectCost:int;
				var data:Object = {Name: "", Price: ""};
				
				for(var i:int = 0; i < this.PriceData.length; i++) 
				{
					if(this.PriceData[i].Name == objectName)
					{
						data.Price = this.PriceData[i].Price
						var guildName:String = objectName.substr(0, objectName.indexOf("_")) + "_Fund"; //adds the _Fund ending to denote membership to a guild
						data.Name = objectName;
						if(!this.messageDispatcher.hasEventListener(GameProgressEvent.LOOKED_UP_DBASE_NAME)) 
						{
							this.messageDispatcher.addEventListener(GameProgressEvent.LOOKED_UP_DBASE_NAME, 
								function (event:GameProgressEvent):void
								{
									var ownerMoney:String = event.ActionData.toString();
									updateInformation(objectName, ownerMoney, data);
								});
						}
						this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.LOOK_UP_DBASE_NAME, -1, guildName));
					}
				}
			};

			private function updateInformation(objectName:String, ownerMoney:String, data:Object):void
			{
			
				var purchasedFrom:String = objectName.substr(0, objectName.indexOf("_"));
				
				try { var objectCost:int = parseInt(data.Price); }
				catch(error:Error) { Alert.show(error.toString()); }
				
				var PlayerMoney:int = parseInt((GameProgressEventManager.AllPlayerInformation[0].Amount));
			
				if(PlayerMoney - objectCost > 0) {
					var BuyerMoney:int =  PlayerMoney - objectCost;
					var myActionData:Object = {Name: "", Price: ""}; //Name of user making the purchase; usually your own name, and price of purchase made.
					//transforms string to integer to calculate new price, then back. Oh, ActionScript data types...=]
					myActionData.Name = this.myUserName;
					myActionData.Price = BuyerMoney.toString();
			
					this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.SUBTRACT_COST, -1, myActionData));
					var OwnerMoney:int = parseInt(ownerMoney) + objectCost;
					
					myActionData.Name = purchasedFrom + "_Fund"; //way to identify a certain buyer
					myActionData.Price = OwnerMoney.toString();
				
					this.messageDispatcher.dispatchEvent(new GameProgressEvent(GameProgressEvent.ADD_COST, -1, myActionData));
					GameProgressEventManager.clearPlayerInfo();
					GameProgressEventManager.ObtainPlayerInfo(this.myUserName);
				} else {
					Alert.show("You don't have enough money, sorry!");
				}