		public static function CreateInsideMarket(messageDispatcher:MessageDispatcher, gameObjectManager:GameObjectManager):World
		{
			var tiles:Array = new Array(2);

			tiles[0] = new Array(10);
			tiles[0][0] = new Array(10);
			tiles[0][0][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][0][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1] = new Array(10);
			tiles[0][1][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][1][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2] = new Array(10);
			tiles[0][2][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][2][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3] = new Array(10);
			tiles[0][3][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][3][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4] = new Array(10);
			tiles[0][4][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][4][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5] = new Array(10);
			tiles[0][5][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][5][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6] = new Array(10);
			tiles[0][6][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][6][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7] = new Array(10);
			tiles[0][7][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][7][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8] = new Array(10);
			tiles[0][8][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][8][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9] = new Array(10);
			tiles[0][9][0] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][1] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][2] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][3] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][4] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][5] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][6] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][7] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][8] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[0][9][9] = new WorldTile(
						"WoodBlock",
						40,
						40,
						0,
						0);
			tiles[1] = new Array(10);
			tiles[1][0] = new Array(10);
			tiles[1][0][0] = null;
			tiles[1][0][1] = null;
			tiles[1][0][2] = null;
			tiles[1][0][3] = null;
			tiles[1][0][4] = null;
			tiles[1][0][5] = null;
			tiles[1][0][6] = null;
			tiles[1][0][7] = null;
			tiles[1][0][8] = null;
			tiles[1][0][9] = null;
			tiles[1][1] = new Array(10);
			tiles[1][1][0] = null;
			tiles[1][1][1] = null;
			tiles[1][1][2] = null;
			tiles[1][1][3] = null;
			tiles[1][1][4] = null;
			tiles[1][1][5] = null;
			tiles[1][1][6] = null;
			tiles[1][1][7] = null;
			tiles[1][1][8] = null;
			tiles[1][1][9] = null;
			tiles[1][2] = new Array(10);
			tiles[1][2][0] = null;
			tiles[1][2][1] = null;
			tiles[1][2][2] = null;
			tiles[1][2][3] = null;
			tiles[1][2][4] = null;
			tiles[1][2][5] = null;
			tiles[1][2][6] = null;
			tiles[1][2][7] = null;
			tiles[1][2][8] = null;
			tiles[1][2][9] = null;
			tiles[1][3] = new Array(10);
			tiles[1][3][0] = null;
			tiles[1][3][1] = null;
			tiles[1][3][2] = null;
			tiles[1][3][3] = null;
			tiles[1][3][4] = new WorldTile(
						"PaPeRo",
						40,
						40,
						0,
						0);
			tiles[1][3][5] = null;
			tiles[1][3][6] = null;
			tiles[1][3][7] = null;
			tiles[1][3][8] = null;
			tiles[1][3][9] = null;
			tiles[1][4] = new Array(10);
			tiles[1][4][0] = null;
			tiles[1][4][1] = null;
			tiles[1][4][2] = null;
			tiles[1][4][3] = null;
			tiles[1][4][4] = null;
			tiles[1][4][5] = null;
			tiles[1][4][6] = null;
			tiles[1][4][7] = null;
			tiles[1][4][8] = null;
			tiles[1][4][9] = null;
			tiles[1][5] = new Array(10);
			tiles[1][5][0] = null;
			tiles[1][5][1] = null;
			tiles[1][5][2] = null;
			tiles[1][5][3] = null;
			tiles[1][5][4] = null;
			tiles[1][5][5] = null;
			tiles[1][5][6] = null;
			tiles[1][5][7] = null;
			tiles[1][5][8] = null;
			tiles[1][5][9] = null;
			tiles[1][6] = new Array(10);
			tiles[1][6][0] = null;
			tiles[1][6][1] = null;
			tiles[1][6][2] = null;
			tiles[1][6][3] = null;
			tiles[1][6][4] = null;
			tiles[1][6][5] = null;
			tiles[1][6][6] = null;
			tiles[1][6][7] = null;
			tiles[1][6][8] = null;
			tiles[1][6][9] = null;
			tiles[1][7] = new Array(10);
			tiles[1][7][0] = null;
			tiles[1][7][1] = null;
			tiles[1][7][2] = null;
			tiles[1][7][3] = null;
			tiles[1][7][4] = null;
			tiles[1][7][5] = null;
			tiles[1][7][6] = null;
			tiles[1][7][7] = null;
			tiles[1][7][8] = null;
			tiles[1][7][9] = null;
			tiles[1][8] = new Array(10);
			tiles[1][8][0] = null;
			tiles[1][8][1] = null;
			tiles[1][8][2] = null;
			tiles[1][8][3] = null;
			tiles[1][8][4] = null;
			tiles[1][8][5] = null;
			tiles[1][8][6] = new WorldTile(
						"PaPeRo",
						40,
						40,
						0,
						0);
			tiles[1][8][7] = null;
			tiles[1][8][8] = null;
			tiles[1][8][9] = null;
			tiles[1][9] = new Array(10);
			tiles[1][9][0] = null;
			tiles[1][9][1] = new WorldTile(
						"Tree Tall",
						40,
						40,
						0,
						0);
			tiles[1][9][2] = new WorldTile(
						"Tree Tall",
						40,
						40,
						0,
						0);
			tiles[1][9][3] = null;
			tiles[1][9][4] = new WorldTile(
						"Tree Tall",
						40,
						40,
						0,
						0);
			tiles[1][9][5] = new WorldTile(
						"Tree Tall",
						40,
						40,
						0,
						0);
			tiles[1][9][6] = null;
			tiles[1][9][7] = null;
			tiles[1][9][8] = null;
			tiles[1][9][9] = null;

			var collision:Array = new Array(10);

			collision[0] = new Array(10);
			collision[0][0] = 0;
			collision[0][1] = 0;
			collision[0][2] = 0;
			collision[0][3] = 0;
			collision[0][4] = 0;
			collision[0][5] = 0;
			collision[0][6] = 0;
			collision[0][7] = 0;
			collision[0][8] = 0;
			collision[0][9] = 0;
			collision[1] = new Array(10);
			collision[1][0] = 0;
			collision[1][1] = 0;
			collision[1][2] = 0;
			collision[1][3] = 0;
			collision[1][4] = 0;
			collision[1][5] = 0;
			collision[1][6] = 0;
			collision[1][7] = 0;
			collision[1][8] = 0;
			collision[1][9] = 0;
			collision[2] = new Array(10);
			collision[2][0] = 0;
			collision[2][1] = 0;
			collision[2][2] = 0;
			collision[2][3] = 0;
			collision[2][4] = 1;
			collision[2][5] = 0;
			collision[2][6] = 0;
			collision[2][7] = 0;
			collision[2][8] = 0;
			collision[2][9] = 0;
			collision[3] = new Array(10);
			collision[3][0] = 0;
			collision[3][1] = 0;
			collision[3][2] = 0;
			collision[3][3] = 0;
			collision[3][4] = 1;
			collision[3][5] = 0;
			collision[3][6] = 0;
			collision[3][7] = 0;
			collision[3][8] = 0;
			collision[3][9] = 0;
			collision[4] = new Array(10);
			collision[4][0] = 0;
			collision[4][1] = 0;
			collision[4][2] = 0;
			collision[4][3] = 0;
			collision[4][4] = 0;
			collision[4][5] = 0;
			collision[4][6] = 0;
			collision[4][7] = 0;
			collision[4][8] = 0;
			collision[4][9] = 0;
			collision[5] = new Array(10);
			collision[5][0] = 0;
			collision[5][1] = 0;
			collision[5][2] = 0;
			collision[5][3] = 0;
			collision[5][4] = 0;
			collision[5][5] = 0;
			collision[5][6] = 0;
			collision[5][7] = 0;
			collision[5][8] = 0;
			collision[5][9] = 0;
			collision[6] = new Array(10);
			collision[6][0] = 0;
			collision[6][1] = 0;
			collision[6][2] = 0;
			collision[6][3] = 0;
			collision[6][4] = 0;
			collision[6][5] = 0;
			collision[6][6] = 0;
			collision[6][7] = 0;
			collision[6][8] = 0;
			collision[6][9] = 0;
			collision[7] = new Array(10);
			collision[7][0] = 0;
			collision[7][1] = 0;
			collision[7][2] = 0;
			collision[7][3] = 0;
			collision[7][4] = 0;
			collision[7][5] = 0;
			collision[7][6] = 1;
			collision[7][7] = 0;
			collision[7][8] = 0;
			collision[7][9] = 0;
			collision[8] = new Array(10);
			collision[8][0] = 0;
			collision[8][1] = 0;
			collision[8][2] = 0;
			collision[8][3] = 0;
			collision[8][4] = 0;
			collision[8][5] = 0;
			collision[8][6] = 1;
			collision[8][7] = 0;
			collision[8][8] = 0;
			collision[8][9] = 0;
			collision[9] = new Array(10);
			collision[9][0] = 0;
			collision[9][1] = 1;
			collision[9][2] = 1;
			collision[9][3] = 0;
			collision[9][4] = 1;
			collision[9][5] = 1;
			collision[9][6] = 0;
			collision[9][7] = 0;
			collision[9][8] = 0;
			collision[9][9] = 0;

			var world:World = new World(40, 40, tiles, collision);

			LevelObjectManager.CreateLevelObject(messageDispatcher, gameObjectManager, world, new Point(360, 120), 10001, 6);
			LevelObjectManager.CreateLevelObject(messageDispatcher, gameObjectManager, world, new Point(80, 160), 1000, 4);
			LevelObjectManager.CreateLevelObject(messageDispatcher, gameObjectManager, world, new Point(280, 240), 1000, 4);

			return world;
		}